#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'essential.ui'
**
** Created: Mon Feb 10 10:22:19 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef ESSENTIAL_H
#define ESSENTIAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_essential
{
public:
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout;
    QCheckBox *adjustEnable;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_6;
    QSlider *hueSlider_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_10;
    QSlider *brightnessSlider_2;
    QLabel *label;
    QCheckBox *brightnessThresholdCheck_2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_9;
    QSlider *contrastSlider_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_7;
    QSlider *saturationSlider_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_8;
    QSlider *gammaSlider_2;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_30;
    QSlider *sharpenSigmaSlider_2;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_51;
    QSlider *grainVarianceSlider_2;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_33;
    QSlider *gradfunRadiusSlider_2;

    void setupUi(QWidget *essential)
    {
        if (essential->objectName().isEmpty())
            essential->setObjectName(QString::fromUtf8("essential"));
        essential->resize(829, 555);
        essential->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        horizontalLayout_12 = new QHBoxLayout(essential);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        adjustEnable = new QCheckBox(essential);
        adjustEnable->setObjectName(QString::fromUtf8("adjustEnable"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        adjustEnable->setFont(font);

        verticalLayout->addWidget(adjustEnable);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_6 = new QLabel(essential);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font);

        horizontalLayout_5->addWidget(label_6);

        hueSlider_2 = new QSlider(essential);
        hueSlider_2->setObjectName(QString::fromUtf8("hueSlider_2"));
        hueSlider_2->setFont(font);
        hueSlider_2->setMaximum(360);
        hueSlider_2->setOrientation(Qt::Horizontal);
        hueSlider_2->setTickPosition(QSlider::TicksBelow);
        hueSlider_2->setTickInterval(60);

        horizontalLayout_5->addWidget(hueSlider_2);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_10 = new QLabel(essential);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font);

        horizontalLayout_3->addWidget(label_10);

        brightnessSlider_2 = new QSlider(essential);
        brightnessSlider_2->setObjectName(QString::fromUtf8("brightnessSlider_2"));
        brightnessSlider_2->setFont(font);
        brightnessSlider_2->setMaximum(200);
        brightnessSlider_2->setOrientation(Qt::Horizontal);
        brightnessSlider_2->setTickPosition(QSlider::TicksBelow);
        brightnessSlider_2->setTickInterval(100);

        horizontalLayout_3->addWidget(brightnessSlider_2);


        verticalLayout->addLayout(horizontalLayout_3);

        label = new QLabel(essential);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        brightnessThresholdCheck_2 = new QCheckBox(essential);
        brightnessThresholdCheck_2->setObjectName(QString::fromUtf8("brightnessThresholdCheck_2"));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        brightnessThresholdCheck_2->setFont(font1);

        verticalLayout->addWidget(brightnessThresholdCheck_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_9 = new QLabel(essential);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font);

        horizontalLayout_4->addWidget(label_9);

        contrastSlider_2 = new QSlider(essential);
        contrastSlider_2->setObjectName(QString::fromUtf8("contrastSlider_2"));
        contrastSlider_2->setFont(font);
        contrastSlider_2->setMaximum(200);
        contrastSlider_2->setOrientation(Qt::Horizontal);
        contrastSlider_2->setTickPosition(QSlider::TicksBelow);
        contrastSlider_2->setTickInterval(100);

        horizontalLayout_4->addWidget(contrastSlider_2);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        horizontalLayout->setSpacing(-1);
#endif
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 0);
        label_7 = new QLabel(essential);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        horizontalLayout->addWidget(label_7);

        saturationSlider_2 = new QSlider(essential);
        saturationSlider_2->setObjectName(QString::fromUtf8("saturationSlider_2"));
        saturationSlider_2->setFont(font);
        saturationSlider_2->setMaximum(300);
        saturationSlider_2->setOrientation(Qt::Horizontal);
        saturationSlider_2->setTickPosition(QSlider::TicksBelow);
        saturationSlider_2->setTickInterval(100);

        horizontalLayout->addWidget(saturationSlider_2);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_8 = new QLabel(essential);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font);

        horizontalLayout_2->addWidget(label_8);

        gammaSlider_2 = new QSlider(essential);
        gammaSlider_2->setObjectName(QString::fromUtf8("gammaSlider_2"));
        gammaSlider_2->setFont(font);
        gammaSlider_2->setMaximum(1000);
        gammaSlider_2->setOrientation(Qt::Horizontal);
        gammaSlider_2->setTickPosition(QSlider::TicksBelow);
        gammaSlider_2->setTickInterval(100);

        horizontalLayout_2->addWidget(gammaSlider_2);


        verticalLayout->addLayout(horizontalLayout_2);


        horizontalLayout_12->addLayout(verticalLayout);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_30 = new QLabel(essential);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setFont(font);

        horizontalLayout_8->addWidget(label_30);

        sharpenSigmaSlider_2 = new QSlider(essential);
        sharpenSigmaSlider_2->setObjectName(QString::fromUtf8("sharpenSigmaSlider_2"));
        sharpenSigmaSlider_2->setFont(font);
        sharpenSigmaSlider_2->setMaximum(200);
        sharpenSigmaSlider_2->setPageStep(10);
        sharpenSigmaSlider_2->setOrientation(Qt::Horizontal);
        sharpenSigmaSlider_2->setTickPosition(QSlider::TicksBelow);
        sharpenSigmaSlider_2->setTickInterval(50);

        horizontalLayout_8->addWidget(sharpenSigmaSlider_2);


        gridLayout->addLayout(horizontalLayout_8, 1, 1, 1, 1);

        checkBox = new QCheckBox(essential);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setFont(font);

        gridLayout->addWidget(checkBox, 0, 1, 1, 1);

        checkBox_2 = new QCheckBox(essential);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setFont(font);

        gridLayout->addWidget(checkBox_2, 3, 1, 1, 1);

        checkBox_3 = new QCheckBox(essential);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setFont(font);

        gridLayout->addWidget(checkBox_3, 5, 1, 1, 1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_51 = new QLabel(essential);
        label_51->setObjectName(QString::fromUtf8("label_51"));
        label_51->setFont(font);

        horizontalLayout_9->addWidget(label_51);

        grainVarianceSlider_2 = new QSlider(essential);
        grainVarianceSlider_2->setObjectName(QString::fromUtf8("grainVarianceSlider_2"));
        grainVarianceSlider_2->setFont(font);
        grainVarianceSlider_2->setMinimum(0);
        grainVarianceSlider_2->setMaximum(40);
        grainVarianceSlider_2->setPageStep(4);
        grainVarianceSlider_2->setOrientation(Qt::Horizontal);
        grainVarianceSlider_2->setTickPosition(QSlider::TicksBelow);
        grainVarianceSlider_2->setTickInterval(4);

        horizontalLayout_9->addWidget(grainVarianceSlider_2);


        gridLayout->addLayout(horizontalLayout_9, 4, 1, 1, 1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_33 = new QLabel(essential);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setFont(font);

        horizontalLayout_10->addWidget(label_33);

        gradfunRadiusSlider_2 = new QSlider(essential);
        gradfunRadiusSlider_2->setObjectName(QString::fromUtf8("gradfunRadiusSlider_2"));
        gradfunRadiusSlider_2->setFont(font);
        gradfunRadiusSlider_2->setMinimum(4);
        gradfunRadiusSlider_2->setMaximum(32);
        gradfunRadiusSlider_2->setPageStep(4);
        gradfunRadiusSlider_2->setOrientation(Qt::Horizontal);
        gradfunRadiusSlider_2->setTickPosition(QSlider::TicksBelow);
        gradfunRadiusSlider_2->setTickInterval(4);

        horizontalLayout_10->addWidget(gradfunRadiusSlider_2);


        gridLayout->addLayout(horizontalLayout_10, 6, 1, 1, 1);


        horizontalLayout_12->addLayout(gridLayout);

#ifndef QT_NO_SHORTCUT
#endif // QT_NO_SHORTCUT

        retranslateUi(essential);

        QMetaObject::connectSlotsByName(essential);
    } // setupUi

    void retranslateUi(QWidget *essential)
    {
        essential->setWindowTitle(Q_("Form", 0));
        adjustEnable->setText(Q_("Adjust Enable", 0));
        label_6->setText(Q_("Hue", 0));
        label_10->setText(Q_("Brightness", 0));
        label->setText(QString());
        brightnessThresholdCheck_2->setText(Q_("Brightness Threshold", 0));
        label_9->setText(Q_("Contrast", 0));
        label_7->setText(Q_("Saturation", 0));
        label_8->setText(Q_("Gamma", 0));
        label_30->setText(Q_("Sigma", 0));
        checkBox->setText(Q_("Sharpen", 0));
        checkBox_2->setText(Q_("Bending removal", 0));
        checkBox_3->setText(Q_("Film grain", 0));
        label_51->setText(Q_("Variance", 0));
        label_33->setText(Q_("Radius", 0));
    } // retranslateUi

};

namespace Ui {
    class essential: public Ui_essential {};
} // namespace Ui

QT_END_NAMESPACE

#endif // ESSENTIAL_H
