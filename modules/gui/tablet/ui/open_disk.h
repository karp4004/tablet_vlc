#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'open_disk.ui'
**
** Created: Mon Feb 10 10:22:18 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef OPEN_DISK_H
#define OPEN_DISK_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpenDisk
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *audioCDRadioButton;
    QRadioButton *bdRadioButton;
    QRadioButton *dvdRadioButton;
    QRadioButton *vcdRadioButton;
    QCheckBox *dvdsimple;
    QHBoxLayout *horizontalLayout_5;
    QLabel *deviceLabel;
    QComboBox *deviceCombo;
    QToolButton *ejectButton;
    QPushButton *browseDiscButton;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *titleLabel;
    QSpinBox *titleSpin;
    QLabel *chapterLabel;
    QSpinBox *chapterSpin;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout;
    QLabel *audioLabel;
    QSpinBox *audioSpin;
    QLabel *subtitlesLabel;
    QSpinBox *subtitlesSpin;

    void setupUi(QWidget *OpenDisk)
    {
        if (OpenDisk->objectName().isEmpty())
            OpenDisk->setObjectName(QString::fromUtf8("OpenDisk"));
        OpenDisk->resize(500, 319);
        OpenDisk->setMinimumSize(QSize(500, 0));
        verticalLayout_2 = new QVBoxLayout(OpenDisk);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(OpenDisk);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        audioCDRadioButton = new QRadioButton(OpenDisk);
        audioCDRadioButton->setObjectName(QString::fromUtf8("audioCDRadioButton"));

        horizontalLayout_2->addWidget(audioCDRadioButton);

        bdRadioButton = new QRadioButton(OpenDisk);
        bdRadioButton->setObjectName(QString::fromUtf8("bdRadioButton"));

        horizontalLayout_2->addWidget(bdRadioButton);

        dvdRadioButton = new QRadioButton(OpenDisk);
        dvdRadioButton->setObjectName(QString::fromUtf8("dvdRadioButton"));
        dvdRadioButton->setLayoutDirection(Qt::LeftToRight);
        dvdRadioButton->setChecked(true);

        horizontalLayout_2->addWidget(dvdRadioButton);

        vcdRadioButton = new QRadioButton(OpenDisk);
        vcdRadioButton->setObjectName(QString::fromUtf8("vcdRadioButton"));

        horizontalLayout_2->addWidget(vcdRadioButton);


        verticalLayout->addLayout(horizontalLayout_2);

        dvdsimple = new QCheckBox(OpenDisk);
        dvdsimple->setObjectName(QString::fromUtf8("dvdsimple"));

        verticalLayout->addWidget(dvdsimple);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        deviceLabel = new QLabel(OpenDisk);
        deviceLabel->setObjectName(QString::fromUtf8("deviceLabel"));

        horizontalLayout_5->addWidget(deviceLabel);

        deviceCombo = new QComboBox(OpenDisk);
        deviceCombo->setObjectName(QString::fromUtf8("deviceCombo"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(deviceCombo->sizePolicy().hasHeightForWidth());
        deviceCombo->setSizePolicy(sizePolicy);
        deviceCombo->setEditable(true);

        horizontalLayout_5->addWidget(deviceCombo);

        ejectButton = new QToolButton(OpenDisk);
        ejectButton->setObjectName(QString::fromUtf8("ejectButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(ejectButton->sizePolicy().hasHeightForWidth());
        ejectButton->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(ejectButton);

        browseDiscButton = new QPushButton(OpenDisk);
        browseDiscButton->setObjectName(QString::fromUtf8("browseDiscButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(browseDiscButton->sizePolicy().hasHeightForWidth());
        browseDiscButton->setSizePolicy(sizePolicy2);

        horizontalLayout_5->addWidget(browseDiscButton);


        verticalLayout->addLayout(horizontalLayout_5);

        label_2 = new QLabel(OpenDisk);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        titleLabel = new QLabel(OpenDisk);
        titleLabel->setObjectName(QString::fromUtf8("titleLabel"));

        horizontalLayout_3->addWidget(titleLabel);

        titleSpin = new QSpinBox(OpenDisk);
        titleSpin->setObjectName(QString::fromUtf8("titleSpin"));
        sizePolicy2.setHeightForWidth(titleSpin->sizePolicy().hasHeightForWidth());
        titleSpin->setSizePolicy(sizePolicy2);
        titleSpin->setAutoFillBackground(false);
        titleSpin->setAlignment(Qt::AlignRight);
        titleSpin->setValue(0);

        horizontalLayout_3->addWidget(titleSpin);

        chapterLabel = new QLabel(OpenDisk);
        chapterLabel->setObjectName(QString::fromUtf8("chapterLabel"));

        horizontalLayout_3->addWidget(chapterLabel);

        chapterSpin = new QSpinBox(OpenDisk);
        chapterSpin->setObjectName(QString::fromUtf8("chapterSpin"));
        sizePolicy2.setHeightForWidth(chapterSpin->sizePolicy().hasHeightForWidth());
        chapterSpin->setSizePolicy(sizePolicy2);
        chapterSpin->setAlignment(Qt::AlignRight);

        horizontalLayout_3->addWidget(chapterSpin);


        verticalLayout->addLayout(horizontalLayout_3);

        label_3 = new QLabel(OpenDisk);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout->addWidget(label_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        audioLabel = new QLabel(OpenDisk);
        audioLabel->setObjectName(QString::fromUtf8("audioLabel"));

        horizontalLayout->addWidget(audioLabel);

        audioSpin = new QSpinBox(OpenDisk);
        audioSpin->setObjectName(QString::fromUtf8("audioSpin"));
        sizePolicy2.setHeightForWidth(audioSpin->sizePolicy().hasHeightForWidth());
        audioSpin->setSizePolicy(sizePolicy2);
        audioSpin->setAlignment(Qt::AlignRight);
        audioSpin->setMinimum(-1);
        audioSpin->setMaximum(10);
        audioSpin->setValue(-1);

        horizontalLayout->addWidget(audioSpin);

        subtitlesLabel = new QLabel(OpenDisk);
        subtitlesLabel->setObjectName(QString::fromUtf8("subtitlesLabel"));

        horizontalLayout->addWidget(subtitlesLabel);

        subtitlesSpin = new QSpinBox(OpenDisk);
        subtitlesSpin->setObjectName(QString::fromUtf8("subtitlesSpin"));
        sizePolicy2.setHeightForWidth(subtitlesSpin->sizePolicy().hasHeightForWidth());
        subtitlesSpin->setSizePolicy(sizePolicy2);
        subtitlesSpin->setAutoFillBackground(false);
        subtitlesSpin->setAlignment(Qt::AlignRight);
        subtitlesSpin->setMinimum(-1);
        subtitlesSpin->setMaximum(31);
        subtitlesSpin->setValue(-1);

        horizontalLayout->addWidget(subtitlesSpin);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(verticalLayout);

#ifndef QT_NO_SHORTCUT
        deviceLabel->setBuddy(deviceCombo);
        titleLabel->setBuddy(titleSpin);
        chapterLabel->setBuddy(chapterSpin);
        audioLabel->setBuddy(audioSpin);
        subtitlesLabel->setBuddy(subtitlesSpin);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(deviceCombo, ejectButton);
        QWidget::setTabOrder(ejectButton, browseDiscButton);

        retranslateUi(OpenDisk);

        QMetaObject::connectSlotsByName(OpenDisk);
    } // setupUi

    void retranslateUi(QWidget *OpenDisk)
    {
        OpenDisk->setWindowTitle(Q_("Form", 0));
        label->setText(Q_("Disc Selection", 0));
        audioCDRadioButton->setText(Q_("Audio CD", 0));
        bdRadioButton->setText(Q_("Blu-ray", 0));
        dvdRadioButton->setText(Q_("DVD", 0));
        vcdRadioButton->setText(Q_("SVCD/VCD", 0));
#ifndef QT_NO_TOOLTIP
        dvdsimple->setToolTip(Q_("Disable Disc Menus", 0));
#endif // QT_NO_TOOLTIP
        dvdsimple->setText(Q_("No disc menus", 0));
        deviceLabel->setText(Q_("Disc device", 0));
        browseDiscButton->setText(Q_("Browse...", 0));
        label_2->setText(Q_("Starting Position", 0));
        titleLabel->setText(Q_("Title", 0));
        chapterLabel->setText(Q_("Chapter", 0));
        label_3->setText(Q_("Audio and Subtitles", 0));
        audioLabel->setText(Q_("Audio track", 0));
        subtitlesLabel->setText(Q_("Subtitle track", 0));
        subtitlesSpin->setSuffix(QString());
    } // retranslateUi

};

namespace Ui {
    class OpenDisk: public Ui_OpenDisk {};
} // namespace Ui

QT_END_NAMESPACE

#endif // OPEN_DISK_H
