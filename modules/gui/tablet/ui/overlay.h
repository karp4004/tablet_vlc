#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'overlay.ui'
**
** Created: Mon Feb 10 10:22:19 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef OVERLAY_H
#define OVERLAY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_overlay
{
public:
    QHBoxLayout *horizontalLayout_10;
    QVBoxLayout *verticalLayout;
    QCheckBox *logoEnable;
    QHBoxLayout *horizontalLayout;
    QLabel *label_19;
    QLineEdit *logoFileText;
    QToolButton *logoBrowseBtn;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_20;
    QSpinBox *logoYSpin;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_21;
    QSpinBox *logoXSpin;
    QLabel *label;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_22;
    QSlider *logoOpacitySlider;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_48;
    QLabel *label_49;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *eraseEnable;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_15;
    QLineEdit *eraseMaskText;
    QToolButton *eraseBrowseBtn;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_16;
    QSpinBox *eraseYSpin;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_17;
    QSpinBox *eraseXSpin;
    QLabel *label_2;
    QCheckBox *marqEnable;
    QLabel *label_23;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_24;
    QComboBox *marqPositionCombo;

    void setupUi(QWidget *overlay)
    {
        if (overlay->objectName().isEmpty())
            overlay->setObjectName(QString::fromUtf8("overlay"));
        overlay->resize(1077, 583);
        overlay->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        horizontalLayout_10 = new QHBoxLayout(overlay);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        logoEnable = new QCheckBox(overlay);
        logoEnable->setObjectName(QString::fromUtf8("logoEnable"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        logoEnable->setFont(font);

        verticalLayout->addWidget(logoEnable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_19 = new QLabel(overlay);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(50, 0));
        label_19->setFont(font);

        horizontalLayout->addWidget(label_19);

        logoFileText = new QLineEdit(overlay);
        logoFileText->setObjectName(QString::fromUtf8("logoFileText"));
        logoFileText->setFont(font);

        horizontalLayout->addWidget(logoFileText);

        logoBrowseBtn = new QToolButton(overlay);
        logoBrowseBtn->setObjectName(QString::fromUtf8("logoBrowseBtn"));
        logoBrowseBtn->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 150);\n"
""));

        horizontalLayout->addWidget(logoBrowseBtn);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_20 = new QLabel(overlay);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(50, 0));
        label_20->setFont(font);

        horizontalLayout_2->addWidget(label_20);

        logoYSpin = new QSpinBox(overlay);
        logoYSpin->setObjectName(QString::fromUtf8("logoYSpin"));
        logoYSpin->setFont(font);
        logoYSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        logoYSpin->setMaximum(4096);

        horizontalLayout_2->addWidget(logoYSpin);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_21 = new QLabel(overlay);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(50, 0));
        label_21->setFont(font);

        horizontalLayout_3->addWidget(label_21);

        logoXSpin = new QSpinBox(overlay);
        logoXSpin->setObjectName(QString::fromUtf8("logoXSpin"));
        logoXSpin->setFont(font);
        logoXSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        logoXSpin->setMaximum(4096);

        horizontalLayout_3->addWidget(logoXSpin);


        verticalLayout->addLayout(horizontalLayout_3);

        label = new QLabel(overlay);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_22 = new QLabel(overlay);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_22->sizePolicy().hasHeightForWidth());
        label_22->setSizePolicy(sizePolicy);
        label_22->setMinimumSize(QSize(50, 0));
        label_22->setFont(font);

        horizontalLayout_4->addWidget(label_22);

        logoOpacitySlider = new QSlider(overlay);
        logoOpacitySlider->setObjectName(QString::fromUtf8("logoOpacitySlider"));
        logoOpacitySlider->setMaximum(255);
        logoOpacitySlider->setOrientation(Qt::Horizontal);
        logoOpacitySlider->setTickPosition(QSlider::TicksBelow);
        logoOpacitySlider->setTickInterval(32);

        horizontalLayout_4->addWidget(logoOpacitySlider);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_48 = new QLabel(overlay);
        label_48->setObjectName(QString::fromUtf8("label_48"));
        label_48->setFont(font);
        label_48->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        horizontalLayout_5->addWidget(label_48);

        label_49 = new QLabel(overlay);
        label_49->setObjectName(QString::fromUtf8("label_49"));
        label_49->setFont(font);
        label_49->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);

        horizontalLayout_5->addWidget(label_49);


        verticalLayout->addLayout(horizontalLayout_5);


        horizontalLayout_10->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        eraseEnable = new QCheckBox(overlay);
        eraseEnable->setObjectName(QString::fromUtf8("eraseEnable"));
        eraseEnable->setFont(font);

        verticalLayout_2->addWidget(eraseEnable);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_15 = new QLabel(overlay);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(50, 0));
        label_15->setFont(font);

        horizontalLayout_6->addWidget(label_15);

        eraseMaskText = new QLineEdit(overlay);
        eraseMaskText->setObjectName(QString::fromUtf8("eraseMaskText"));
        eraseMaskText->setFont(font);

        horizontalLayout_6->addWidget(eraseMaskText);

        eraseBrowseBtn = new QToolButton(overlay);
        eraseBrowseBtn->setObjectName(QString::fromUtf8("eraseBrowseBtn"));
        eraseBrowseBtn->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 150);"));

        horizontalLayout_6->addWidget(eraseBrowseBtn);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_16 = new QLabel(overlay);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(50, 0));
        label_16->setFont(font);

        horizontalLayout_7->addWidget(label_16);

        eraseYSpin = new QSpinBox(overlay);
        eraseYSpin->setObjectName(QString::fromUtf8("eraseYSpin"));
        eraseYSpin->setFont(font);
        eraseYSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        eraseYSpin->setMaximum(4096);

        horizontalLayout_7->addWidget(eraseYSpin);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_17 = new QLabel(overlay);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setMinimumSize(QSize(50, 0));
        label_17->setFont(font);

        horizontalLayout_8->addWidget(label_17);

        eraseXSpin = new QSpinBox(overlay);
        eraseXSpin->setObjectName(QString::fromUtf8("eraseXSpin"));
        eraseXSpin->setFont(font);
        eraseXSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        eraseXSpin->setMaximum(4096);

        horizontalLayout_8->addWidget(eraseXSpin);


        verticalLayout_2->addLayout(horizontalLayout_8);

        label_2 = new QLabel(overlay);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        marqEnable = new QCheckBox(overlay);
        marqEnable->setObjectName(QString::fromUtf8("marqEnable"));
        marqEnable->setFont(font);

        verticalLayout_2->addWidget(marqEnable);

        label_23 = new QLabel(overlay);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setFont(font);

        verticalLayout_2->addWidget(label_23);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_24 = new QLabel(overlay);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setFont(font);

        horizontalLayout_9->addWidget(label_24);

        marqPositionCombo = new QComboBox(overlay);
        marqPositionCombo->setObjectName(QString::fromUtf8("marqPositionCombo"));
        marqPositionCombo->setFont(font);

        horizontalLayout_9->addWidget(marqPositionCombo);


        verticalLayout_2->addLayout(horizontalLayout_9);


        horizontalLayout_10->addLayout(verticalLayout_2);

#ifndef QT_NO_SHORTCUT
        label_19->setBuddy(logoFileText);
        label_20->setBuddy(logoYSpin);
        label_21->setBuddy(logoXSpin);
        label_22->setBuddy(logoOpacitySlider);
        label_48->setBuddy(logoOpacitySlider);
        label_49->setBuddy(logoOpacitySlider);
        label_15->setBuddy(eraseMaskText);
        label_16->setBuddy(eraseYSpin);
        label_17->setBuddy(eraseXSpin);
        label_24->setBuddy(marqPositionCombo);
#endif // QT_NO_SHORTCUT

        retranslateUi(overlay);

        QMetaObject::connectSlotsByName(overlay);
    } // setupUi

    void retranslateUi(QWidget *overlay)
    {
        overlay->setWindowTitle(Q_("Form", 0));
        logoEnable->setText(Q_("Add logo", 0));
        label_19->setText(Q_("Logo", 0));
        logoBrowseBtn->setText(Q_("...", 0));
        label_20->setText(Q_("Top", 0));
        logoYSpin->setSuffix(Q_(" px", 0));
        label_21->setText(Q_("Left", 0));
        logoXSpin->setSuffix(Q_(" px", 0));
        label->setText(QString());
        label_22->setText(Q_("Transparency", 0));
        label_48->setText(Q_("none", 0));
        label_49->setText(Q_("full", 0));
        eraseEnable->setText(Q_("Logo erase", 0));
        label_15->setText(Q_("Mask", 0));
        eraseBrowseBtn->setText(Q_("...", 0));
        label_16->setText(Q_("Top", 0));
        eraseYSpin->setSuffix(Q_(" px", 0));
        label_17->setText(Q_("Left", 0));
        eraseXSpin->setSuffix(Q_(" px", 0));
        label_2->setText(QString());
        marqEnable->setText(Q_("Add text", 0));
        label_23->setText(Q_("Text", 0));
        label_24->setText(Q_("Position", 0));
    } // retranslateUi

};

namespace Ui {
    class overlay: public Ui_overlay {};
} // namespace Ui

QT_END_NAMESPACE

#endif // OVERLAY_H
