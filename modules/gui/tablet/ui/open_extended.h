#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'open_extended.ui'
**
** Created: Mon Feb 10 10:22:18 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef OPEN_EXTENDED_H
#define OPEN_EXTENDED_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QTimeEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_open_extended
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_3;
    QTimeEdit *startTimeTimeEdit;
    QHBoxLayout *horizontalLayout_2;
    QLabel *cacheLabel;
    QSpinBox *cacheSpinBox;
    QHBoxLayout *horizontalLayout_3;
    QLabel *advancedLabel;
    QLineEdit *mrlLine;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QLineEdit *advancedLineInput;
    QLabel *label_2;
    QCheckBox *slaveCheckbox;
    QHBoxLayout *horizontalLayout;
    QLabel *slaveLabel;
    QLineEdit *slaveText;
    QPushButton *slaveBrowseButton;

    void setupUi(QWidget *open_extended)
    {
        if (open_extended->objectName().isEmpty())
            open_extended->setObjectName(QString::fromUtf8("open_extended"));
        open_extended->resize(446, 585);
        open_extended->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(open_extended);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_3 = new QLabel(open_extended);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_3);

        startTimeTimeEdit = new QTimeEdit(open_extended);
        startTimeTimeEdit->setObjectName(QString::fromUtf8("startTimeTimeEdit"));
        sizePolicy.setHeightForWidth(startTimeTimeEdit->sizePolicy().hasHeightForWidth());
        startTimeTimeEdit->setSizePolicy(sizePolicy);
        startTimeTimeEdit->setFont(font);
        startTimeTimeEdit->setStyleSheet(QString::fromUtf8(""));
        startTimeTimeEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        startTimeTimeEdit->setCurrentSection(QDateTimeEdit::HourSection);
        startTimeTimeEdit->setTimeSpec(Qt::LocalTime);

        horizontalLayout_5->addWidget(startTimeTimeEdit);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        cacheLabel = new QLabel(open_extended);
        cacheLabel->setObjectName(QString::fromUtf8("cacheLabel"));
        sizePolicy.setHeightForWidth(cacheLabel->sizePolicy().hasHeightForWidth());
        cacheLabel->setSizePolicy(sizePolicy);
        cacheLabel->setFont(font);
        cacheLabel->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(cacheLabel);

        cacheSpinBox = new QSpinBox(open_extended);
        cacheSpinBox->setObjectName(QString::fromUtf8("cacheSpinBox"));
        sizePolicy.setHeightForWidth(cacheSpinBox->sizePolicy().hasHeightForWidth());
        cacheSpinBox->setSizePolicy(sizePolicy);
        cacheSpinBox->setFont(font);
        cacheSpinBox->setStyleSheet(QString::fromUtf8(""));
        cacheSpinBox->setAlignment(Qt::AlignRight);
        cacheSpinBox->setMaximum(65535);
        cacheSpinBox->setSingleStep(100);

        horizontalLayout_2->addWidget(cacheSpinBox);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        advancedLabel = new QLabel(open_extended);
        advancedLabel->setObjectName(QString::fromUtf8("advancedLabel"));
        sizePolicy.setHeightForWidth(advancedLabel->sizePolicy().hasHeightForWidth());
        advancedLabel->setSizePolicy(sizePolicy);
        advancedLabel->setFont(font);
        advancedLabel->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(advancedLabel);

        mrlLine = new QLineEdit(open_extended);
        mrlLine->setObjectName(QString::fromUtf8("mrlLine"));
        sizePolicy.setHeightForWidth(mrlLine->sizePolicy().hasHeightForWidth());
        mrlLine->setSizePolicy(sizePolicy);
        mrlLine->setStyleSheet(QString::fromUtf8(""));
        mrlLine->setReadOnly(true);

        horizontalLayout_3->addWidget(mrlLine);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label = new QLabel(open_extended);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(label);

        advancedLineInput = new QLineEdit(open_extended);
        advancedLineInput->setObjectName(QString::fromUtf8("advancedLineInput"));
        sizePolicy.setHeightForWidth(advancedLineInput->sizePolicy().hasHeightForWidth());
        advancedLineInput->setSizePolicy(sizePolicy);
        advancedLineInput->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_4->addWidget(advancedLineInput);


        verticalLayout->addLayout(horizontalLayout_4);

        label_2 = new QLabel(open_extended);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        slaveCheckbox = new QCheckBox(open_extended);
        slaveCheckbox->setObjectName(QString::fromUtf8("slaveCheckbox"));
        slaveCheckbox->setFont(font);

        verticalLayout->addWidget(slaveCheckbox);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        slaveLabel = new QLabel(open_extended);
        slaveLabel->setObjectName(QString::fromUtf8("slaveLabel"));
        sizePolicy.setHeightForWidth(slaveLabel->sizePolicy().hasHeightForWidth());
        slaveLabel->setSizePolicy(sizePolicy);
        slaveLabel->setFont(font);
        slaveLabel->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(slaveLabel);

        slaveText = new QLineEdit(open_extended);
        slaveText->setObjectName(QString::fromUtf8("slaveText"));
        sizePolicy.setHeightForWidth(slaveText->sizePolicy().hasHeightForWidth());
        slaveText->setSizePolicy(sizePolicy);
        slaveText->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(slaveText);

        slaveBrowseButton = new QPushButton(open_extended);
        slaveBrowseButton->setObjectName(QString::fromUtf8("slaveBrowseButton"));
        sizePolicy.setHeightForWidth(slaveBrowseButton->sizePolicy().hasHeightForWidth());
        slaveBrowseButton->setSizePolicy(sizePolicy);
        slaveBrowseButton->setFont(font);
        slaveBrowseButton->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(slaveBrowseButton);

        horizontalLayout->setStretch(0, 2);
        horizontalLayout->setStretch(1, 1);
        horizontalLayout->setStretch(2, 1);

        verticalLayout->addLayout(horizontalLayout);

#ifndef QT_NO_SHORTCUT
        label_3->setBuddy(startTimeTimeEdit);
        cacheLabel->setBuddy(cacheSpinBox);
        advancedLabel->setBuddy(mrlLine);
        label->setBuddy(advancedLineInput);
        slaveLabel->setBuddy(slaveText);
#endif // QT_NO_SHORTCUT

        retranslateUi(open_extended);

        QMetaObject::connectSlotsByName(open_extended);
    } // setupUi

    void retranslateUi(QWidget *open_extended)
    {
        open_extended->setWindowTitle(Q_("Form", 0));
        label_3->setText(Q_("Start Time", 0));
#ifndef QT_NO_TOOLTIP
        startTimeTimeEdit->setToolTip(Q_("Change the start time for the media", 0));
#endif // QT_NO_TOOLTIP
        startTimeTimeEdit->setDisplayFormat(Q_("HH'H':mm'm':ss's'.zzz", 0));
        cacheLabel->setText(Q_("Caching", 0));
#ifndef QT_NO_TOOLTIP
        cacheSpinBox->setToolTip(Q_("Change the caching for the media", 0));
#endif // QT_NO_TOOLTIP
        cacheSpinBox->setSuffix(Q_(" ms", 0));
        advancedLabel->setText(Q_("MRL", 0));
        label->setText(Q_("Edit Options", 0));
#ifndef QT_NO_TOOLTIP
        advancedLineInput->setToolTip(Q_("Complete MRL for VLC internal", 0));
#endif // QT_NO_TOOLTIP
        label_2->setText(QString());
        slaveCheckbox->setText(Q_("Play another media synchronously", 0));
        slaveLabel->setText(Q_("Extra media", 0));
#ifndef QT_NO_TOOLTIP
        slaveBrowseButton->setToolTip(Q_("Select the file", 0));
#endif // QT_NO_TOOLTIP
        slaveBrowseButton->setText(Q_("Browse...", 0));
    } // retranslateUi

};

namespace Ui {
    class open_extended: public Ui_open_extended {};
} // namespace Ui

QT_END_NAMESPACE

#endif // OPEN_EXTENDED_H
