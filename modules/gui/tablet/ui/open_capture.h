#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'open_capture.ui'
**
** Created: Mon Feb 10 10:22:18 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef OPEN_CAPTURE_H
#define OPEN_CAPTURE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpenCapture
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *deviceCombo;

    void setupUi(QWidget *OpenCapture)
    {
        if (OpenCapture->objectName().isEmpty())
            OpenCapture->setObjectName(QString::fromUtf8("OpenCapture"));
        OpenCapture->resize(760, 561);
        OpenCapture->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        verticalLayout = new QVBoxLayout(OpenCapture);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(OpenCapture);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        deviceCombo = new QComboBox(OpenCapture);
        deviceCombo->setObjectName(QString::fromUtf8("deviceCombo"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(deviceCombo->sizePolicy().hasHeightForWidth());
        deviceCombo->setSizePolicy(sizePolicy);
        deviceCombo->setFont(font);
        deviceCombo->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 150);"));

        horizontalLayout->addWidget(deviceCombo);


        verticalLayout->addLayout(horizontalLayout);

        verticalLayout->setStretch(0, 1);

        retranslateUi(OpenCapture);

        QMetaObject::connectSlotsByName(OpenCapture);
    } // setupUi

    void retranslateUi(QWidget *OpenCapture)
    {
        label->setText(Q_("Capture mode", 0));
#ifndef QT_NO_TOOLTIP
        deviceCombo->setToolTip(Q_("Select the capture device type", 0));
#endif // QT_NO_TOOLTIP
        Q_UNUSED(OpenCapture);
    } // retranslateUi

};

namespace Ui {
    class OpenCapture: public Ui_OpenCapture {};
} // namespace Ui

QT_END_NAMESPACE

#endif // OPEN_CAPTURE_H
