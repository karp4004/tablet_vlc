#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'crop.ui'
**
** Created: Mon Feb 10 10:22:19 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef CROP_H
#define CROP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_crop
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_10;
    QSpinBox *cropTopPx;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_15;
    QSpinBox *cropTopPx_4;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_14;
    QSpinBox *cropTopPx_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_13;
    QSpinBox *cropTopPx_2;
    QCheckBox *topBotCropSync;
    QCheckBox *leftRightCropSync;

    void setupUi(QWidget *crop)
    {
        if (crop->objectName().isEmpty())
            crop->setObjectName(QString::fromUtf8("crop"));
        crop->resize(766, 531);
        crop->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        verticalLayout = new QVBoxLayout(crop);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_10 = new QLabel(crop);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        label_10->setFont(font);

        horizontalLayout->addWidget(label_10);

        cropTopPx = new QSpinBox(crop);
        cropTopPx->setObjectName(QString::fromUtf8("cropTopPx"));
        cropTopPx->setFont(font);
        cropTopPx->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cropTopPx->setMaximum(4095);

        horizontalLayout->addWidget(cropTopPx);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 8);

        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_15 = new QLabel(crop);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font);

        horizontalLayout_4->addWidget(label_15);

        cropTopPx_4 = new QSpinBox(crop);
        cropTopPx_4->setObjectName(QString::fromUtf8("cropTopPx_4"));
        cropTopPx_4->setFont(font);
        cropTopPx_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cropTopPx_4->setMaximum(4095);

        horizontalLayout_4->addWidget(cropTopPx_4);

        horizontalLayout_4->setStretch(0, 1);
        horizontalLayout_4->setStretch(1, 8);

        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_14 = new QLabel(crop);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font);

        horizontalLayout_3->addWidget(label_14);

        cropTopPx_3 = new QSpinBox(crop);
        cropTopPx_3->setObjectName(QString::fromUtf8("cropTopPx_3"));
        cropTopPx_3->setFont(font);
        cropTopPx_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cropTopPx_3->setMaximum(4095);

        horizontalLayout_3->addWidget(cropTopPx_3);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 8);

        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_13 = new QLabel(crop);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setFont(font);

        horizontalLayout_2->addWidget(label_13);

        cropTopPx_2 = new QSpinBox(crop);
        cropTopPx_2->setObjectName(QString::fromUtf8("cropTopPx_2"));
        cropTopPx_2->setFont(font);
        cropTopPx_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cropTopPx_2->setMaximum(4095);

        horizontalLayout_2->addWidget(cropTopPx_2);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(1, 8);

        verticalLayout->addLayout(horizontalLayout_2);

        topBotCropSync = new QCheckBox(crop);
        topBotCropSync->setObjectName(QString::fromUtf8("topBotCropSync"));
        topBotCropSync->setFont(font);

        verticalLayout->addWidget(topBotCropSync);

        leftRightCropSync = new QCheckBox(crop);
        leftRightCropSync->setObjectName(QString::fromUtf8("leftRightCropSync"));
        leftRightCropSync->setFont(font);

        verticalLayout->addWidget(leftRightCropSync);

        leftRightCropSync->raise();
        topBotCropSync->raise();
#ifndef QT_NO_SHORTCUT
        label_10->setBuddy(cropTopPx);
        label_15->setBuddy(cropTopPx);
        label_14->setBuddy(cropTopPx);
        label_13->setBuddy(cropTopPx);
#endif // QT_NO_SHORTCUT

        retranslateUi(crop);

        QMetaObject::connectSlotsByName(crop);
    } // setupUi

    void retranslateUi(QWidget *crop)
    {
        crop->setWindowTitle(Q_("Form", 0));
        label_10->setText(Q_("Top", 0));
        cropTopPx->setSuffix(Q_(" px", 0));
        label_15->setText(Q_("Left", 0));
        cropTopPx_4->setSuffix(Q_(" px", 0));
        label_14->setText(Q_("Right", 0));
        cropTopPx_3->setSuffix(Q_(" px", 0));
        label_13->setText(Q_("Bottom", 0));
        cropTopPx_2->setSuffix(Q_(" px", 0));
        topBotCropSync->setText(Q_("Synchronize top and bottom", 0));
        leftRightCropSync->setText(Q_("Synchronize left and right", 0));
    } // retranslateUi

};

namespace Ui {
    class crop: public Ui_crop {};
} // namespace Ui

QT_END_NAMESPACE

#endif // CROP_H
