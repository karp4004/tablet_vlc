#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'advanced.ui'
**
** Created: Mon Feb 10 10:22:19 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef ADVANCED_H
#define ADVANCED_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_advanced
{
public:
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout;
    QCheckBox *antiflickerEnable;
    QHBoxLayout *horizontalLayout;
    QLabel *label_37;
    QSlider *antiflickerSofteningSizeSlider_2;
    QCheckBox *motionblurEnable;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_38;
    QSlider *blurFactorSlider_2;
    QCheckBox *gaussianblurEnable;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_39;
    QSlider *gaussianbluSigmaSlider_2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *waveEnable;
    QCheckBox *rippleEnable;
    QCheckBox *psychedelicEnable;
    QCheckBox *mirrorEnable;
    QCheckBox *motiondetectEnable;
    QCheckBox *anaglyphEnable;
    QCheckBox *cloneEnable;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_10;
    QSpinBox *cloneCountSpin_2;

    void setupUi(QWidget *advanced)
    {
        if (advanced->objectName().isEmpty())
            advanced->setObjectName(QString::fromUtf8("advanced"));
        advanced->resize(733, 458);
        advanced->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        horizontalLayout_5 = new QHBoxLayout(advanced);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        antiflickerEnable = new QCheckBox(advanced);
        antiflickerEnable->setObjectName(QString::fromUtf8("antiflickerEnable"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        antiflickerEnable->setFont(font);

        verticalLayout->addWidget(antiflickerEnable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_37 = new QLabel(advanced);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setFont(font);

        horizontalLayout->addWidget(label_37);

        antiflickerSofteningSizeSlider_2 = new QSlider(advanced);
        antiflickerSofteningSizeSlider_2->setObjectName(QString::fromUtf8("antiflickerSofteningSizeSlider_2"));
        antiflickerSofteningSizeSlider_2->setMinimum(0);
        antiflickerSofteningSizeSlider_2->setMaximum(100);
        antiflickerSofteningSizeSlider_2->setValue(80);
        antiflickerSofteningSizeSlider_2->setOrientation(Qt::Horizontal);
        antiflickerSofteningSizeSlider_2->setTickPosition(QSlider::TicksBelow);
        antiflickerSofteningSizeSlider_2->setTickInterval(16);

        horizontalLayout->addWidget(antiflickerSofteningSizeSlider_2);


        verticalLayout->addLayout(horizontalLayout);

        motionblurEnable = new QCheckBox(advanced);
        motionblurEnable->setObjectName(QString::fromUtf8("motionblurEnable"));
        motionblurEnable->setFont(font);

        verticalLayout->addWidget(motionblurEnable);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_38 = new QLabel(advanced);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setFont(font);

        horizontalLayout_2->addWidget(label_38);

        blurFactorSlider_2 = new QSlider(advanced);
        blurFactorSlider_2->setObjectName(QString::fromUtf8("blurFactorSlider_2"));
        blurFactorSlider_2->setMinimum(1);
        blurFactorSlider_2->setMaximum(127);
        blurFactorSlider_2->setValue(80);
        blurFactorSlider_2->setOrientation(Qt::Horizontal);
        blurFactorSlider_2->setTickPosition(QSlider::TicksBelow);
        blurFactorSlider_2->setTickInterval(16);

        horizontalLayout_2->addWidget(blurFactorSlider_2);


        verticalLayout->addLayout(horizontalLayout_2);

        gaussianblurEnable = new QCheckBox(advanced);
        gaussianblurEnable->setObjectName(QString::fromUtf8("gaussianblurEnable"));
        gaussianblurEnable->setFont(font);

        verticalLayout->addWidget(gaussianblurEnable);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_39 = new QLabel(advanced);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setFont(font);

        horizontalLayout_3->addWidget(label_39);

        gaussianbluSigmaSlider_2 = new QSlider(advanced);
        gaussianbluSigmaSlider_2->setObjectName(QString::fromUtf8("gaussianbluSigmaSlider_2"));
        gaussianbluSigmaSlider_2->setMinimum(1);
        gaussianbluSigmaSlider_2->setMaximum(127);
        gaussianbluSigmaSlider_2->setValue(80);
        gaussianbluSigmaSlider_2->setOrientation(Qt::Horizontal);
        gaussianbluSigmaSlider_2->setTickPosition(QSlider::TicksBelow);
        gaussianbluSigmaSlider_2->setTickInterval(16);

        horizontalLayout_3->addWidget(gaussianbluSigmaSlider_2);


        verticalLayout->addLayout(horizontalLayout_3);


        horizontalLayout_5->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        waveEnable = new QCheckBox(advanced);
        waveEnable->setObjectName(QString::fromUtf8("waveEnable"));
        waveEnable->setFont(font);

        verticalLayout_2->addWidget(waveEnable);

        rippleEnable = new QCheckBox(advanced);
        rippleEnable->setObjectName(QString::fromUtf8("rippleEnable"));
        rippleEnable->setFont(font);

        verticalLayout_2->addWidget(rippleEnable);

        psychedelicEnable = new QCheckBox(advanced);
        psychedelicEnable->setObjectName(QString::fromUtf8("psychedelicEnable"));
        psychedelicEnable->setFont(font);

        verticalLayout_2->addWidget(psychedelicEnable);

        mirrorEnable = new QCheckBox(advanced);
        mirrorEnable->setObjectName(QString::fromUtf8("mirrorEnable"));
        mirrorEnable->setFont(font);

        verticalLayout_2->addWidget(mirrorEnable);

        motiondetectEnable = new QCheckBox(advanced);
        motiondetectEnable->setObjectName(QString::fromUtf8("motiondetectEnable"));
        motiondetectEnable->setFont(font);

        verticalLayout_2->addWidget(motiondetectEnable);

        anaglyphEnable = new QCheckBox(advanced);
        anaglyphEnable->setObjectName(QString::fromUtf8("anaglyphEnable"));
        anaglyphEnable->setFont(font);

        verticalLayout_2->addWidget(anaglyphEnable);

        cloneEnable = new QCheckBox(advanced);
        cloneEnable->setObjectName(QString::fromUtf8("cloneEnable"));
        cloneEnable->setFont(font);

        verticalLayout_2->addWidget(cloneEnable);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_10 = new QLabel(advanced);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font);

        horizontalLayout_4->addWidget(label_10);

        cloneCountSpin_2 = new QSpinBox(advanced);
        cloneCountSpin_2->setObjectName(QString::fromUtf8("cloneCountSpin_2"));
        cloneCountSpin_2->setFont(font);
        cloneCountSpin_2->setMinimum(1);
        cloneCountSpin_2->setValue(2);

        horizontalLayout_4->addWidget(cloneCountSpin_2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        horizontalLayout_5->addLayout(verticalLayout_2);

#ifndef QT_NO_SHORTCUT
#endif // QT_NO_SHORTCUT

        retranslateUi(advanced);

        QMetaObject::connectSlotsByName(advanced);
    } // setupUi

    void retranslateUi(QWidget *advanced)
    {
        advanced->setWindowTitle(Q_("Form", 0));
        antiflickerEnable->setText(Q_("Anti-flickering", 0));
        label_37->setText(Q_("Soften", 0));
        motionblurEnable->setText(Q_("Motion blur", 0));
        label_38->setText(Q_("Factor", 0));
        gaussianblurEnable->setText(Q_("Spatial blur", 0));
        label_39->setText(Q_("Sigma", 0));
        waveEnable->setText(Q_("Waves", 0));
        rippleEnable->setText(Q_("Water effect", 0));
        psychedelicEnable->setText(Q_("Psychedelic", 0));
        mirrorEnable->setText(Q_("Mirror", 0));
        motiondetectEnable->setText(Q_("Motion detect", 0));
        anaglyphEnable->setText(Q_("Anaglyph 3D", 0));
        cloneEnable->setText(Q_("Clone", 0));
        label_10->setText(Q_("Number of clones", 0));
    } // retranslateUi

};

namespace Ui {
    class advanced: public Ui_advanced {};
} // namespace Ui

QT_END_NAMESPACE

#endif // ADVANCED_H
