#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'open_net.ui'
**
** Created: Mon Feb 10 10:22:18 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef OPEN_NET_H
#define OPEN_NET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpenNetwork
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *urlComboBox;
    QLabel *examples;

    void setupUi(QWidget *OpenNetwork)
    {
        if (OpenNetwork->objectName().isEmpty())
            OpenNetwork->setObjectName(QString::fromUtf8("OpenNetwork"));
        OpenNetwork->resize(471, 224);
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 0));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        OpenNetwork->setPalette(palette);
        QFont font;
        font.setFamily(QString::fromUtf8("URW Bookman L"));
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        OpenNetwork->setFont(font);
        OpenNetwork->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(OpenNetwork);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_2 = new QLabel(OpenNetwork);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(OpenNetwork);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8(""));
        label->setTextFormat(Qt::AutoText);

        horizontalLayout->addWidget(label);

        urlComboBox = new QComboBox(OpenNetwork);
        urlComboBox->setObjectName(QString::fromUtf8("urlComboBox"));
        sizePolicy.setHeightForWidth(urlComboBox->sizePolicy().hasHeightForWidth());
        urlComboBox->setSizePolicy(sizePolicy);
        urlComboBox->setMaximumSize(QSize(400, 16777215));
        urlComboBox->setFont(font);
        urlComboBox->setStyleSheet(QString::fromUtf8(""));
        urlComboBox->setEditable(true);
        urlComboBox->setInsertPolicy(QComboBox::NoInsert);
        urlComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
        urlComboBox->setMinimumContentsLength(0);

        horizontalLayout->addWidget(urlComboBox);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 4);

        verticalLayout->addLayout(horizontalLayout);

        examples = new QLabel(OpenNetwork);
        examples->setObjectName(QString::fromUtf8("examples"));
        examples->setFont(font);
        examples->setStyleSheet(QString::fromUtf8(""));
        examples->setText(QString::fromUtf8("http://www.example.com/stream.avi \n"
"rtp://@:1234\n"
"mms://mms.examples.com/stream.as\n"
"rtsp://server.example.org:8080/test.sdp"));
        examples->setMargin(5);
        examples->setIndent(10);

        verticalLayout->addWidget(examples);


        retranslateUi(OpenNetwork);

        urlComboBox->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(OpenNetwork);
    } // setupUi

    void retranslateUi(QWidget *OpenNetwork)
    {
        OpenNetwork->setWindowTitle(Q_("Form", 0));
        label_2->setText(Q_("Stream selection", 0));
        label->setText(Q_("URI:", 0));
    } // retranslateUi

};

namespace Ui {
    class OpenNetwork: public Ui_OpenNetwork {};
} // namespace Ui

QT_END_NAMESPACE

#endif // OPEN_NET_H
