#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'geomerty.ui'
**
** Created: Mon Feb 10 10:22:19 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef GEOMERTY_H
#define GEOMERTY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDial>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_geomerty
{
public:
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout;
    QCheckBox *rotateEnable;
    QHBoxLayout *horizontalLayout;
    QLabel *label_28;
    QDial *rotateAngleDial;
    QCheckBox *transformEnable;
    QComboBox *transformTypeCombo;
    QCheckBox *magnifyEnable;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *puzzleEnable;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_27;
    QSpinBox *puzzleRowsSpin;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_26;
    QSpinBox *puzzleColsSpin;
    QCheckBox *puzzleBlackSlotCheck;
    QLabel *label;
    QCheckBox *wallEnable;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_31;
    QSpinBox *wallRowsSpin;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_30;
    QSpinBox *wallColsSpin;

    void setupUi(QWidget *geomerty)
    {
        if (geomerty->objectName().isEmpty())
            geomerty->setObjectName(QString::fromUtf8("geomerty"));
        geomerty->resize(741, 530);
        geomerty->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        horizontalLayout_6 = new QHBoxLayout(geomerty);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        rotateEnable = new QCheckBox(geomerty);
        rotateEnable->setObjectName(QString::fromUtf8("rotateEnable"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        rotateEnable->setFont(font);

        verticalLayout->addWidget(rotateEnable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_28 = new QLabel(geomerty);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_28->sizePolicy().hasHeightForWidth());
        label_28->setSizePolicy(sizePolicy);
        label_28->setFont(font);

        horizontalLayout->addWidget(label_28);

        rotateAngleDial = new QDial(geomerty);
        rotateAngleDial->setObjectName(QString::fromUtf8("rotateAngleDial"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(rotateAngleDial->sizePolicy().hasHeightForWidth());
        rotateAngleDial->setSizePolicy(sizePolicy1);
        rotateAngleDial->setMaximum(359);
        rotateAngleDial->setValue(180);
        rotateAngleDial->setSliderPosition(180);

        horizontalLayout->addWidget(rotateAngleDial);


        verticalLayout->addLayout(horizontalLayout);

        transformEnable = new QCheckBox(geomerty);
        transformEnable->setObjectName(QString::fromUtf8("transformEnable"));
        transformEnable->setFont(font);

        verticalLayout->addWidget(transformEnable);

        transformTypeCombo = new QComboBox(geomerty);
        transformTypeCombo->setObjectName(QString::fromUtf8("transformTypeCombo"));

        verticalLayout->addWidget(transformTypeCombo);

        magnifyEnable = new QCheckBox(geomerty);
        magnifyEnable->setObjectName(QString::fromUtf8("magnifyEnable"));
        magnifyEnable->setFont(font);

        verticalLayout->addWidget(magnifyEnable);


        horizontalLayout_6->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        puzzleEnable = new QCheckBox(geomerty);
        puzzleEnable->setObjectName(QString::fromUtf8("puzzleEnable"));
        puzzleEnable->setFont(font);

        verticalLayout_2->addWidget(puzzleEnable);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_27 = new QLabel(geomerty);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setFont(font);

        horizontalLayout_2->addWidget(label_27);

        puzzleRowsSpin = new QSpinBox(geomerty);
        puzzleRowsSpin->setObjectName(QString::fromUtf8("puzzleRowsSpin"));
        puzzleRowsSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        puzzleRowsSpin->setMinimum(2);
        puzzleRowsSpin->setMaximum(16);
        puzzleRowsSpin->setValue(4);

        horizontalLayout_2->addWidget(puzzleRowsSpin);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_26 = new QLabel(geomerty);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setFont(font);

        horizontalLayout_3->addWidget(label_26);

        puzzleColsSpin = new QSpinBox(geomerty);
        puzzleColsSpin->setObjectName(QString::fromUtf8("puzzleColsSpin"));
        puzzleColsSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        puzzleColsSpin->setMinimum(2);
        puzzleColsSpin->setMaximum(16);
        puzzleColsSpin->setValue(4);

        horizontalLayout_3->addWidget(puzzleColsSpin);


        verticalLayout_2->addLayout(horizontalLayout_3);

        puzzleBlackSlotCheck = new QCheckBox(geomerty);
        puzzleBlackSlotCheck->setObjectName(QString::fromUtf8("puzzleBlackSlotCheck"));
        puzzleBlackSlotCheck->setFont(font);

        verticalLayout_2->addWidget(puzzleBlackSlotCheck);

        label = new QLabel(geomerty);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        wallEnable = new QCheckBox(geomerty);
        wallEnable->setObjectName(QString::fromUtf8("wallEnable"));
        wallEnable->setFont(font);

        verticalLayout_2->addWidget(wallEnable);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_31 = new QLabel(geomerty);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setFont(font);

        horizontalLayout_4->addWidget(label_31);

        wallRowsSpin = new QSpinBox(geomerty);
        wallRowsSpin->setObjectName(QString::fromUtf8("wallRowsSpin"));
        wallRowsSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        wallRowsSpin->setMinimum(1);
        wallRowsSpin->setValue(3);

        horizontalLayout_4->addWidget(wallRowsSpin);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_30 = new QLabel(geomerty);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setFont(font);

        horizontalLayout_5->addWidget(label_30);

        wallColsSpin = new QSpinBox(geomerty);
        wallColsSpin->setObjectName(QString::fromUtf8("wallColsSpin"));
        wallColsSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        wallColsSpin->setMinimum(1);
        wallColsSpin->setValue(3);

        horizontalLayout_5->addWidget(wallColsSpin);


        verticalLayout_2->addLayout(horizontalLayout_5);


        horizontalLayout_6->addLayout(verticalLayout_2);

#ifndef QT_NO_SHORTCUT
        label_28->setBuddy(rotateAngleDial);
        label_27->setBuddy(puzzleRowsSpin);
        label_26->setBuddy(puzzleColsSpin);
        label_31->setBuddy(wallRowsSpin);
        label_30->setBuddy(wallColsSpin);
#endif // QT_NO_SHORTCUT

        retranslateUi(geomerty);

        QMetaObject::connectSlotsByName(geomerty);
    } // setupUi

    void retranslateUi(QWidget *geomerty)
    {
        geomerty->setWindowTitle(Q_("Form", 0));
        rotateEnable->setText(Q_("Rotate", 0));
        label_28->setText(Q_("Angle", 0));
        transformEnable->setText(Q_("Transfom", 0));
        magnifyEnable->setText(Q_("Interactive Zoom", 0));
        puzzleEnable->setText(Q_("Puzzle", 0));
        label_27->setText(Q_("Rows", 0));
        label_26->setText(Q_("Columns", 0));
        puzzleBlackSlotCheck->setText(Q_("Black Slot", 0));
        label->setText(QString());
        wallEnable->setText(Q_("Wall", 0));
        label_31->setText(Q_("Rows", 0));
        label_30->setText(Q_("Columns", 0));
    } // retranslateUi

};

namespace Ui {
    class geomerty: public Ui_geomerty {};
} // namespace Ui

QT_END_NAMESPACE

#endif // GEOMERTY_H
