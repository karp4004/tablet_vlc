#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'colors.ui'
**
** Created: Mon Feb 10 10:22:19 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef COLORS_H
#define COLORS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Colors
{
public:
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *colorthresEnable;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_13;
    QSlider *colorthresSaturationthresSlider;
    QHBoxLayout *horizontalLayout_5;
    QWidget *widget_3;
    QLabel *label_14;
    QSlider *colorthresSimilaritythresSlider;
    QLabel *label;
    QCheckBox *extractEnable;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_6;
    QLineEdit *extractComponentText;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_8;
    QLineEdit *colorthresColorText;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *posterizeEnable;
    QCheckBox *sepiaEnable;
    QHBoxLayout *horizontalLayout;
    QLabel *label_18;
    QSpinBox *sepiaIntensitySpin;
    QLabel *label_2;
    QCheckBox *gradientEnable;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_25;
    QComboBox *gradientModeCombo;
    QHBoxLayout *horizontalLayout_7;
    QCheckBox *gradientTypeCheck;
    QCheckBox *gradientCartoonCheck;
    QCheckBox *invertEnable;

    void setupUi(QWidget *Colors)
    {
        if (Colors->objectName().isEmpty())
            Colors->setObjectName(QString::fromUtf8("Colors"));
        Colors->resize(820, 598);
        Colors->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        horizontalLayout_8 = new QHBoxLayout(Colors);
        horizontalLayout_8->setSpacing(-1);
#ifndef Q_OS_MAC
        horizontalLayout_8->setContentsMargins(9, 9, 9, 9);
#endif
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        colorthresEnable = new QCheckBox(Colors);
        colorthresEnable->setObjectName(QString::fromUtf8("colorthresEnable"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        colorthresEnable->setFont(font);

        verticalLayout_2->addWidget(colorthresEnable);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_13 = new QLabel(Colors);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_13->sizePolicy().hasHeightForWidth());
        label_13->setSizePolicy(sizePolicy);
        label_13->setFont(font);

        horizontalLayout_4->addWidget(label_13);

        colorthresSaturationthresSlider = new QSlider(Colors);
        colorthresSaturationthresSlider->setObjectName(QString::fromUtf8("colorthresSaturationthresSlider"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(colorthresSaturationthresSlider->sizePolicy().hasHeightForWidth());
        colorthresSaturationthresSlider->setSizePolicy(sizePolicy1);
        colorthresSaturationthresSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_4->addWidget(colorthresSaturationthresSlider);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        widget_3 = new QWidget(Colors);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));

        horizontalLayout_5->addWidget(widget_3);

        label_14 = new QLabel(Colors);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font);

        horizontalLayout_5->addWidget(label_14);

        colorthresSimilaritythresSlider = new QSlider(Colors);
        colorthresSimilaritythresSlider->setObjectName(QString::fromUtf8("colorthresSimilaritythresSlider"));
        sizePolicy1.setHeightForWidth(colorthresSimilaritythresSlider->sizePolicy().hasHeightForWidth());
        colorthresSimilaritythresSlider->setSizePolicy(sizePolicy1);
        colorthresSimilaritythresSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(colorthresSimilaritythresSlider);


        verticalLayout_2->addLayout(horizontalLayout_5);

        label = new QLabel(Colors);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        extractEnable = new QCheckBox(Colors);
        extractEnable->setObjectName(QString::fromUtf8("extractEnable"));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        extractEnable->setFont(font1);

        verticalLayout_2->addWidget(extractEnable);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_6 = new QLabel(Colors);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy2);
        label_6->setFont(font);

        horizontalLayout_2->addWidget(label_6);

        extractComponentText = new QLineEdit(Colors);
        extractComponentText->setObjectName(QString::fromUtf8("extractComponentText"));
        extractComponentText->setMaximumSize(QSize(140, 16777215));
        extractComponentText->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(extractComponentText);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_8 = new QLabel(Colors);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        sizePolicy2.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy2);
        label_8->setFont(font);

        horizontalLayout_3->addWidget(label_8);

        colorthresColorText = new QLineEdit(Colors);
        colorthresColorText->setObjectName(QString::fromUtf8("colorthresColorText"));
        colorthresColorText->setMaximumSize(QSize(140, 16777215));
        colorthresColorText->setFont(font);
        colorthresColorText->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(colorthresColorText);


        verticalLayout_2->addLayout(horizontalLayout_3);


        horizontalLayout_8->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        posterizeEnable = new QCheckBox(Colors);
        posterizeEnable->setObjectName(QString::fromUtf8("posterizeEnable"));
        posterizeEnable->setFont(font);

        verticalLayout_3->addWidget(posterizeEnable);

        sepiaEnable = new QCheckBox(Colors);
        sepiaEnable->setObjectName(QString::fromUtf8("sepiaEnable"));
        sepiaEnable->setFont(font);

        verticalLayout_3->addWidget(sepiaEnable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_18 = new QLabel(Colors);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setFont(font);

        horizontalLayout->addWidget(label_18);

        sepiaIntensitySpin = new QSpinBox(Colors);
        sepiaIntensitySpin->setObjectName(QString::fromUtf8("sepiaIntensitySpin"));
        sepiaIntensitySpin->setFont(font);
        sepiaIntensitySpin->setMaximum(255);
        sepiaIntensitySpin->setValue(30);

        horizontalLayout->addWidget(sepiaIntensitySpin);


        verticalLayout_3->addLayout(horizontalLayout);

        label_2 = new QLabel(Colors);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_3->addWidget(label_2);

        gradientEnable = new QCheckBox(Colors);
        gradientEnable->setObjectName(QString::fromUtf8("gradientEnable"));
        gradientEnable->setFont(font);

        verticalLayout_3->addWidget(gradientEnable);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_25 = new QLabel(Colors);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setFont(font);

        horizontalLayout_6->addWidget(label_25);

        gradientModeCombo = new QComboBox(Colors);
        gradientModeCombo->setObjectName(QString::fromUtf8("gradientModeCombo"));
        sizePolicy1.setHeightForWidth(gradientModeCombo->sizePolicy().hasHeightForWidth());
        gradientModeCombo->setSizePolicy(sizePolicy1);
        gradientModeCombo->setFont(font);

        horizontalLayout_6->addWidget(gradientModeCombo);


        verticalLayout_3->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        gradientTypeCheck = new QCheckBox(Colors);
        gradientTypeCheck->setObjectName(QString::fromUtf8("gradientTypeCheck"));
        gradientTypeCheck->setFont(font);

        horizontalLayout_7->addWidget(gradientTypeCheck);

        gradientCartoonCheck = new QCheckBox(Colors);
        gradientCartoonCheck->setObjectName(QString::fromUtf8("gradientCartoonCheck"));
        gradientCartoonCheck->setFont(font);

        horizontalLayout_7->addWidget(gradientCartoonCheck);


        verticalLayout_3->addLayout(horizontalLayout_7);

        invertEnable = new QCheckBox(Colors);
        invertEnable->setObjectName(QString::fromUtf8("invertEnable"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(invertEnable->sizePolicy().hasHeightForWidth());
        invertEnable->setSizePolicy(sizePolicy3);
        invertEnable->setFont(font);

        verticalLayout_3->addWidget(invertEnable);


        horizontalLayout_8->addLayout(verticalLayout_3);

#ifndef QT_NO_SHORTCUT
        label_13->setBuddy(colorthresSaturationthresSlider);
        label_14->setBuddy(colorthresSimilaritythresSlider);
        label_6->setBuddy(extractComponentText);
        label_8->setBuddy(colorthresColorText);
#endif // QT_NO_SHORTCUT

        retranslateUi(Colors);

        QMetaObject::connectSlotsByName(Colors);
    } // setupUi

    void retranslateUi(QWidget *Colors)
    {
        Colors->setWindowTitle(Q_("Form", 0));
        colorthresEnable->setText(Q_("Color threshold", 0));
        label_13->setText(Q_("Saturation", 0));
        label_14->setText(Q_("Similarity", 0));
        label->setText(QString());
        extractEnable->setText(Q_("Color Extraction", 0));
        label_6->setText(Q_("Color", 0));
        extractComponentText->setInputMask(Q_(">HHHHHH;#", 0));
        label_8->setText(Q_("Color", 0));
        colorthresColorText->setInputMask(Q_(">HHHHHH;#", 0));
        posterizeEnable->setText(Q_("Posterize", 0));
        sepiaEnable->setText(Q_("Sepia", 0));
        label_18->setText(Q_("Intensity", 0));
        label_2->setText(QString());
        gradientEnable->setText(Q_("Gradient", 0));
        label_25->setText(Q_("Mode", 0));
        gradientTypeCheck->setText(Q_("Color", 0));
        gradientCartoonCheck->setText(Q_("Cartoon", 0));
        invertEnable->setText(Q_("Negate colors", 0));
    } // retranslateUi

};

namespace Ui {
    class Colors: public Ui_Colors {};
} // namespace Ui

QT_END_NAMESPACE

#endif // COLORS_H
