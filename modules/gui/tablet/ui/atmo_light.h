#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'atmo_light.ui'
**
** Created: Mon Feb 10 10:22:19 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef ATMO_LIGHT_H
#define ATMO_LIGHT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_atmo_light
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QCheckBox *atmoEnable;
    QHBoxLayout *horizontalLayout;
    QLabel *label_37;
    QSlider *atmoEdgeweightningSlider;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_38;
    QSlider *atmoBrightnessSlider;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_39;
    QSlider *atmoDarknesslimitSlider;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_41;
    QSlider *atmoMeanlengthSlider;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_42;
    QSlider *atmoMeanthresholdSlider;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_43;
    QSlider *atmoPercentnewSlider;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_40;
    QComboBox *atmoFiltermodeCombo;
    QCheckBox *atmoShowdotsCheck;

    void setupUi(QWidget *atmo_light)
    {
        if (atmo_light->objectName().isEmpty())
            atmo_light->setObjectName(QString::fromUtf8("atmo_light"));
        atmo_light->resize(763, 579);
        atmo_light->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        verticalLayout_2 = new QVBoxLayout(atmo_light);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        atmoEnable = new QCheckBox(atmo_light);
        atmoEnable->setObjectName(QString::fromUtf8("atmoEnable"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        atmoEnable->setFont(font);

        verticalLayout->addWidget(atmoEnable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_37 = new QLabel(atmo_light);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setFont(font);

        horizontalLayout->addWidget(label_37);

        atmoEdgeweightningSlider = new QSlider(atmo_light);
        atmoEdgeweightningSlider->setObjectName(QString::fromUtf8("atmoEdgeweightningSlider"));
        atmoEdgeweightningSlider->setMinimum(1);
        atmoEdgeweightningSlider->setMaximum(30);
        atmoEdgeweightningSlider->setPageStep(5);
        atmoEdgeweightningSlider->setValue(3);
        atmoEdgeweightningSlider->setOrientation(Qt::Horizontal);
        atmoEdgeweightningSlider->setTickPosition(QSlider::TicksBelow);
        atmoEdgeweightningSlider->setTickInterval(3);

        horizontalLayout->addWidget(atmoEdgeweightningSlider);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_38 = new QLabel(atmo_light);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setFont(font);

        horizontalLayout_2->addWidget(label_38);

        atmoBrightnessSlider = new QSlider(atmo_light);
        atmoBrightnessSlider->setObjectName(QString::fromUtf8("atmoBrightnessSlider"));
        atmoBrightnessSlider->setMinimum(50);
        atmoBrightnessSlider->setMaximum(300);
        atmoBrightnessSlider->setPageStep(10);
        atmoBrightnessSlider->setValue(100);
        atmoBrightnessSlider->setOrientation(Qt::Horizontal);
        atmoBrightnessSlider->setTickPosition(QSlider::TicksBelow);
        atmoBrightnessSlider->setTickInterval(10);

        horizontalLayout_2->addWidget(atmoBrightnessSlider);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_39 = new QLabel(atmo_light);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setFont(font);

        horizontalLayout_3->addWidget(label_39);

        atmoDarknesslimitSlider = new QSlider(atmo_light);
        atmoDarknesslimitSlider->setObjectName(QString::fromUtf8("atmoDarknesslimitSlider"));
        atmoDarknesslimitSlider->setMinimum(0);
        atmoDarknesslimitSlider->setMaximum(10);
        atmoDarknesslimitSlider->setPageStep(10);
        atmoDarknesslimitSlider->setValue(3);
        atmoDarknesslimitSlider->setOrientation(Qt::Horizontal);
        atmoDarknesslimitSlider->setTickPosition(QSlider::TicksBelow);
        atmoDarknesslimitSlider->setTickInterval(1);

        horizontalLayout_3->addWidget(atmoDarknesslimitSlider);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_41 = new QLabel(atmo_light);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setFont(font);

        horizontalLayout_4->addWidget(label_41);

        atmoMeanlengthSlider = new QSlider(atmo_light);
        atmoMeanlengthSlider->setObjectName(QString::fromUtf8("atmoMeanlengthSlider"));
        atmoMeanlengthSlider->setMinimum(300);
        atmoMeanlengthSlider->setMaximum(5000);
        atmoMeanlengthSlider->setSingleStep(10);
        atmoMeanlengthSlider->setPageStep(50);
        atmoMeanlengthSlider->setValue(300);
        atmoMeanlengthSlider->setOrientation(Qt::Horizontal);
        atmoMeanlengthSlider->setTickPosition(QSlider::TicksBelow);
        atmoMeanlengthSlider->setTickInterval(200);

        horizontalLayout_4->addWidget(atmoMeanlengthSlider);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_42 = new QLabel(atmo_light);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setFont(font);

        horizontalLayout_5->addWidget(label_42);

        atmoMeanthresholdSlider = new QSlider(atmo_light);
        atmoMeanthresholdSlider->setObjectName(QString::fromUtf8("atmoMeanthresholdSlider"));
        atmoMeanthresholdSlider->setMinimum(1);
        atmoMeanthresholdSlider->setMaximum(100);
        atmoMeanthresholdSlider->setSingleStep(1);
        atmoMeanthresholdSlider->setPageStep(10);
        atmoMeanthresholdSlider->setValue(40);
        atmoMeanthresholdSlider->setOrientation(Qt::Horizontal);
        atmoMeanthresholdSlider->setTickPosition(QSlider::TicksBelow);
        atmoMeanthresholdSlider->setTickInterval(10);

        horizontalLayout_5->addWidget(atmoMeanthresholdSlider);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_43 = new QLabel(atmo_light);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setFont(font);

        horizontalLayout_6->addWidget(label_43);

        atmoPercentnewSlider = new QSlider(atmo_light);
        atmoPercentnewSlider->setObjectName(QString::fromUtf8("atmoPercentnewSlider"));
        atmoPercentnewSlider->setMinimum(1);
        atmoPercentnewSlider->setMaximum(100);
        atmoPercentnewSlider->setSingleStep(1);
        atmoPercentnewSlider->setPageStep(10);
        atmoPercentnewSlider->setValue(50);
        atmoPercentnewSlider->setOrientation(Qt::Horizontal);
        atmoPercentnewSlider->setTickPosition(QSlider::TicksBelow);
        atmoPercentnewSlider->setTickInterval(10);

        horizontalLayout_6->addWidget(atmoPercentnewSlider);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_40 = new QLabel(atmo_light);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_40->sizePolicy().hasHeightForWidth());
        label_40->setSizePolicy(sizePolicy);
        label_40->setFont(font);

        horizontalLayout_7->addWidget(label_40);

        atmoFiltermodeCombo = new QComboBox(atmo_light);
        atmoFiltermodeCombo->setObjectName(QString::fromUtf8("atmoFiltermodeCombo"));
        atmoFiltermodeCombo->setFont(font);

        horizontalLayout_7->addWidget(atmoFiltermodeCombo);


        verticalLayout->addLayout(horizontalLayout_7);

        atmoShowdotsCheck = new QCheckBox(atmo_light);
        atmoShowdotsCheck->setObjectName(QString::fromUtf8("atmoShowdotsCheck"));
        atmoShowdotsCheck->setFont(font);

        verticalLayout->addWidget(atmoShowdotsCheck);


        verticalLayout_2->addLayout(verticalLayout);

#ifndef QT_NO_SHORTCUT
        label_37->setBuddy(atmoEdgeweightningSlider);
        label_38->setBuddy(atmoBrightnessSlider);
        label_39->setBuddy(atmoDarknesslimitSlider);
        label_41->setBuddy(atmoMeanlengthSlider);
        label_42->setBuddy(atmoMeanthresholdSlider);
        label_43->setBuddy(atmoPercentnewSlider);
        label_40->setBuddy(atmoFiltermodeCombo);
#endif // QT_NO_SHORTCUT

        retranslateUi(atmo_light);

        QMetaObject::connectSlotsByName(atmo_light);
    } // setupUi

    void retranslateUi(QWidget *atmo_light)
    {
        atmo_light->setWindowTitle(Q_("Form", 0));
        atmoEnable->setText(Q_("Atmo light", 0));
        label_37->setText(Q_("Edge weightning", 0));
        label_38->setText(Q_("Brightness (%)", 0));
        label_39->setText(Q_("Darkness limit", 0));
        label_41->setText(Q_("Filter length (ms)", 0));
        label_42->setText(Q_("Filter threshold (%)", 0));
        label_43->setText(Q_("Filter smoothness (%)", 0));
        label_40->setText(Q_("Output Color Filtermode", 0));
        atmoShowdotsCheck->setText(Q_("Mark analyzed Pixels", 0));
    } // retranslateUi

};

namespace Ui {
    class atmo_light: public Ui_atmo_light {};
} // namespace Ui

QT_END_NAMESPACE

#endif // ATMO_LIGHT_H
