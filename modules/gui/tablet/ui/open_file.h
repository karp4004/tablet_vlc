#define Q_(a,b) QString::fromUtf8(_(a))
/********************************************************************************
** Form generated from reading UI file 'open_file.ui'
**
** Created: Mon Feb 10 10:22:18 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef OPEN_FILE_H
#define OPEN_FILE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpenFile
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *tempWidget;
    QGridLayout *gridLayout;
    QLabel *label;
    QListWidget *fileListWidg;
    QPushButton *removeFileButton;
    QPushButton *fileBrowseButton;
    QLabel *label_2;
    QGroupBox *subGroupBox;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *subInput;
    QPushButton *subBrowseButton;

    void setupUi(QWidget *OpenFile)
    {
        if (OpenFile->objectName().isEmpty())
            OpenFile->setObjectName(QString::fromUtf8("OpenFile"));
        OpenFile->resize(683, 401);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(OpenFile->sizePolicy().hasHeightForWidth());
        OpenFile->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        OpenFile->setFont(font);
        OpenFile->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(OpenFile);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tempWidget = new QGroupBox(OpenFile);
        tempWidget->setObjectName(QString::fromUtf8("tempWidget"));
        tempWidget->setFont(font);
        tempWidget->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        tempWidget->setFlat(false);
        tempWidget->setCheckable(false);
        tempWidget->setChecked(false);
        gridLayout = new QGridLayout(tempWidget);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(tempWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        fileListWidg = new QListWidget(tempWidget);
        fileListWidg->setObjectName(QString::fromUtf8("fileListWidg"));
        sizePolicy.setHeightForWidth(fileListWidg->sizePolicy().hasHeightForWidth());
        fileListWidg->setSizePolicy(sizePolicy);
        fileListWidg->setMaximumSize(QSize(16777215, 16777215));
        fileListWidg->setFont(font);
        fileListWidg->setContextMenuPolicy(Qt::DefaultContextMenu);
        fileListWidg->setStyleSheet(QString::fromUtf8(""));
        fileListWidg->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        fileListWidg->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        fileListWidg->setEditTriggers(QAbstractItemView::AllEditTriggers);
        fileListWidg->setAlternatingRowColors(true);

        gridLayout->addWidget(fileListWidg, 2, 0, 3, 2);

        removeFileButton = new QPushButton(tempWidget);
        removeFileButton->setObjectName(QString::fromUtf8("removeFileButton"));
        removeFileButton->setEnabled(false);
        sizePolicy.setHeightForWidth(removeFileButton->sizePolicy().hasHeightForWidth());
        removeFileButton->setSizePolicy(sizePolicy);
        removeFileButton->setFont(font);
        removeFileButton->setStyleSheet(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/playlist_remove"), QSize(), QIcon::Normal, QIcon::Off);
        removeFileButton->setIcon(icon);

        gridLayout->addWidget(removeFileButton, 3, 2, 1, 1);

        fileBrowseButton = new QPushButton(tempWidget);
        fileBrowseButton->setObjectName(QString::fromUtf8("fileBrowseButton"));
        sizePolicy.setHeightForWidth(fileBrowseButton->sizePolicy().hasHeightForWidth());
        fileBrowseButton->setSizePolicy(sizePolicy);
        fileBrowseButton->setFont(font);
        fileBrowseButton->setStyleSheet(QString::fromUtf8(""));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/playlist_add"), QSize(), QIcon::Normal, QIcon::Off);
        fileBrowseButton->setIcon(icon1);

        gridLayout->addWidget(fileBrowseButton, 2, 2, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setRowStretch(2, 8);
        gridLayout->setRowStretch(3, 8);
        gridLayout->setColumnStretch(0, 3);

        verticalLayout->addWidget(tempWidget);

        label_2 = new QLabel(OpenFile);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        subGroupBox = new QGroupBox(OpenFile);
        subGroupBox->setObjectName(QString::fromUtf8("subGroupBox"));
        sizePolicy.setHeightForWidth(subGroupBox->sizePolicy().hasHeightForWidth());
        subGroupBox->setSizePolicy(sizePolicy);
        subGroupBox->setFont(font);
        subGroupBox->setCheckable(true);
        subGroupBox->setChecked(false);
        horizontalLayout_2 = new QHBoxLayout(subGroupBox);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        subInput = new QLineEdit(subGroupBox);
        subInput->setObjectName(QString::fromUtf8("subInput"));
        sizePolicy.setHeightForWidth(subInput->sizePolicy().hasHeightForWidth());
        subInput->setSizePolicy(sizePolicy);
        subInput->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_2->addWidget(subInput);

        subBrowseButton = new QPushButton(subGroupBox);
        subBrowseButton->setObjectName(QString::fromUtf8("subBrowseButton"));
        sizePolicy.setHeightForWidth(subBrowseButton->sizePolicy().hasHeightForWidth());
        subBrowseButton->setSizePolicy(sizePolicy);
        subBrowseButton->setMinimumSize(QSize(100, 0));
        subBrowseButton->setFont(font);
        subBrowseButton->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_2->addWidget(subBrowseButton);

        horizontalLayout_2->setStretch(0, 4);
        horizontalLayout_2->setStretch(1, 1);

        verticalLayout->addWidget(subGroupBox);

        QWidget::setTabOrder(fileListWidg, fileBrowseButton);
        QWidget::setTabOrder(fileBrowseButton, removeFileButton);

        retranslateUi(OpenFile);

        QMetaObject::connectSlotsByName(OpenFile);
    } // setupUi

    void retranslateUi(QWidget *OpenFile)
    {
        OpenFile->setWindowTitle(Q_("Open File", 0));
#ifndef QT_NO_TOOLTIP
        tempWidget->setToolTip(Q_("Choose one or more media file to open", 0));
#endif // QT_NO_TOOLTIP
        tempWidget->setTitle(QString());
        label->setText(Q_("File selection", 0));
        removeFileButton->setText(Q_("Remove", 0));
        fileBrowseButton->setText(Q_("Add...", 0));
        label_2->setText(Q_("Subtitle file selection", 0));
        subGroupBox->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        subBrowseButton->setToolTip(Q_("Select the subtitle file", 0));
#endif // QT_NO_TOOLTIP
        subBrowseButton->setText(Q_("Browse...", 0));
    } // retranslateUi

};

namespace Ui {
    class OpenFile: public Ui_OpenFile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // OPEN_FILE_H
