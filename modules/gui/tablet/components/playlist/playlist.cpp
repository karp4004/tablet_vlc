/*****************************************************************************
 * playlist.cpp : Custom widgets for the playlist
 ****************************************************************************
 * Copyright © 2007-2010 the VideoLAN team
 * $Id: cb8173ea290a826729d0c106c714c68a1057a37c $
 *
 * Authors: Clément Stenac <zorglub@videolan.org>
 *          Jean-Baptiste Kempf <jb@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * ( at your option ) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "components/playlist/playlist.hpp"
#include "components/playlist/standardpanel.hpp"  /* MainView */
#include "components/playlist/selector.hpp"       /* PLSelector */
#include "components/playlist/playlist_model.hpp" /* PLModel */
#include "components/playlist/ml_model.hpp"       /* MLModel */
#include "components/interface_widgets.hpp"       /* CoverArtLabel */

#include "util/searchlineedit.hpp"

#include "input_manager.hpp"                      /* art signal */
#include "main_interface.hpp"                     /* DropEvent TODO remove this*/

#include <QMenu>
#include <QSignalMapper>
#include <QSlider>
#include <QStackedWidget>

/**********************************************************************
 * Playlist Widget. The embedded playlist
 **********************************************************************/

PlaylistWidget::PlaylistWidget( intf_thread_t *_p_i, QWidget *_par )
               : QWidget( _par ), p_intf ( _p_i )
{
    QGridLayout *layout = new QGridLayout( this );
    layout->setMargin( 0 ); layout->setSpacing( 0 );

    QFont f;
    f.setPointSize(20);

    selector = new PLSelector( this, f, p_intf );
    selector->setFont(f);

    QPalette palette;
    palette.setColor(QPalette::Window,QColor(0,0,0,0));
    palette.setColor(QPalette::Base,QColor(0,0,0,0));
    selector->setPalette(palette);
//
//    selector->setStyleSheet(
//		"background-color:qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,"
//                                 "stop: 0 #FF92BB, stop: 1 white);"
////    	"QTreeView::item {"
////			 "border: 1px solid #d9d9d9;"
////			 "border-top-color: transparent;"
////			 "border-bottom-color: transparent;"
////    		"background-color:#transparent;"
////		"}"
//	);

    selector->setStyleSheet("QTreeView::item:selected { "
    		"background-color:rgba(255, 255, 255, 150);"
    		"}" );
//      selector->setStyleSheet("QTreeView::item { background-color:green;}" );

    playlist_t * p_playlist = THEPL;
    PL_LOCK;
    playlist_item_t *p_root = p_playlist->p_playing;
    PL_UNLOCK;
    PLModel *model = PLModel::getPLModel( p_intf );
#ifdef MEDIA_LIBRARY
    MLModel *mlmodel = new MLModel( p_intf, this );
    mainView = new StandardPLPanel( this, p_intf, p_root, selector, model, mlmodel );
#else
    mainView = new StandardPLPanel( this, p_intf, p_root, selector, model, NULL );
#endif
    mainView->setRootItem( p_root, false );
    QPalette palette2;
    palette2.setColor(QPalette::Window,QColor(255, 255, 255, 150));
    palette2.setColor(QPalette::Base,QColor(255, 255, 255, 150));
    //mainView->setPalette(palette2);

    locationBar = new LocationBar( model );
    locationBar->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Preferred );

    QToolButton *viewButton = new QToolButton( this );
    QIcon icon = style()->standardIcon( QStyle::SP_FileDialogDetailedView );
    viewButton->setIcon( icon );
    viewButton->setToolTip( qtr("Change playlistview") );
    viewButton->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );
    viewButton->setMenu( StandardPLPanel::viewSelectionMenu( mainView ));

    searchEdit = new SearchLineEdit( this );
    searchEdit->setToolTip( qtr("Search the playlist") );
    searchEdit->setFont(f);
    searchEdit->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );

    setAcceptDrops( true );
    setWindowTitle( qtr( "Playlist" ) );
    setWindowRole( "vlc-playlist" );
    setWindowIcon( QApplication::windowIcon() );

    layout->addWidget( selector, 0, 0, 10, 3 );

    layout->addWidget( mainView, 0, 3, 8, 7 );
    layout->addWidget( searchEdit, 8, 3, 2, 5 );
    layout->addWidget( viewButton, 8, 8, 2, 2 );

    CONNECT( locationBar, invoked( const QModelIndex & ),
             mainView, browseInto( const QModelIndex & ) );

    CONNECT( viewButton, clicked(), mainView, cycleViews() );

    CONNECT( searchEdit, textChanged( const QString& ),
             mainView, search( const QString& ) );
    CONNECT( searchEdit, searchDelayedChanged( const QString& ),
             mainView, searchDelayed( const QString & ) );

    CONNECT( mainView, viewChanged( const QModelIndex& ),
             this, changeView( const QModelIndex &) );

    DCONNECT( selector, categoryActivated( playlist_item_t *, bool ),
              mainView, setRootItem( playlist_item_t *, bool ) );

    CONNECT( selector, SDCategorySelected(bool), mainView, setWaiting(bool) );
}

PlaylistWidget::~PlaylistWidget()
{
    msg_Dbg( p_intf, "Playlist Destroyed" );
}

void PlaylistWidget::dropEvent( QDropEvent *event )
{
	if(selector)
	{
		if( !( selector->getCurrentItemCategory() == IS_PL ||
			   selector->getCurrentItemCategory() == IS_ML ) ) return;

		if( p_intf->p_sys->p_mi )
			p_intf->p_sys->p_mi->dropEventPlay( event, false,
					(selector->getCurrentItemCategory() == IS_PL) );
	}
}
void PlaylistWidget::dragEnterEvent( QDragEnterEvent *event )
{
    event->acceptProposedAction();
}

void PlaylistWidget::closeEvent( QCloseEvent *event )
{
    if( THEDP->isDying() )
    {
        p_intf->p_sys->p_mi->playlistVisible = true;
        event->accept();
    }
    else
    {
        p_intf->p_sys->p_mi->playlistVisible = false;
        hide();
        event->ignore();
    }
}

void PlaylistWidget::forceHide()
{
	if(leftSplitter)
	{
		leftSplitter->hide();
	}

	if(mainView)
	{
		mainView->hide();
	}

	updateGeometry();
}

void PlaylistWidget::forceShow()
{
	if(leftSplitter)
	{
		leftSplitter->show();
	}

	if(mainView)
	{
		mainView->show();
	}

	updateGeometry();
}

void PlaylistWidget::changeView( const QModelIndex& index )
{
	if(searchEdit)
	{
		searchEdit->clear();
	}

	if(locationBar)
	{
		locationBar->setIndex( index );
	}
}

void PlaylistWidget::clearPlaylist()
{
    PLModel::getPLModel( p_intf )->clearPlaylist();
}
#include <QSignalMapper>
#include <QMenu>
#include <QPainter>
LocationBar::LocationBar( PLModel *m )
{
    model = m;
    mapper = new QSignalMapper( this );
    CONNECT( mapper, mapped( int ), this, invoke( int ) );

    btnMore = new LocationButton( "...", false, true, this );
    menuMore = new QMenu( this );
    btnMore->setMenu( menuMore );
}

void LocationBar::setIndex( const QModelIndex &index )
{
    qDeleteAll( buttons );
    buttons.clear();
    qDeleteAll( actions );
    actions.clear();

    QModelIndex i = index;
    bool first = true;

    while( true )
    {
        QString text = model->getTitle( i );

        QAbstractButton *btn = new LocationButton( text, first, !first, this );
        btn->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Fixed );
        buttons.append( btn );

        QAction *action = new QAction( text, this );
        actions.append( action );
        CONNECT( btn, clicked(), action, trigger() );

        mapper->setMapping( action, model->itemId( i ) );
        CONNECT( action, triggered(), mapper, map() );

        first = false;

        if( i.isValid() ) i = i.parent();
        else break;
    }

    QString prefix;
    for( int a = actions.count() - 1; a >= 0 ; a-- )
    {
        actions[a]->setText( prefix + actions[a]->text() );
        prefix += QString("  ");
    }

    if( isVisible() ) layOut( size() );
}

void LocationBar::setRootIndex()
{
    setIndex( QModelIndex() );
}

void LocationBar::invoke( int i_id )
{
    QModelIndex index = model->index( i_id, 0 );
    emit invoked ( index );
}

void LocationBar::layOut( const QSize& size )
{
    menuMore->clear();
    widths.clear();

    int count = buttons.count();
    int totalWidth = 0;
    for( int i = 0; i < count; i++ )
    {
        int w = buttons[i]->sizeHint().width();
        widths.append( w );
        totalWidth += w;
        if( totalWidth > size.width() ) break;
    }

    int x = 0;
    int shown = widths.count();

    if( totalWidth > size.width() && count > 1 )
    {
        QSize sz = btnMore->sizeHint();
        btnMore->setGeometry( 0, 0, sz.width(), size.height() );
        btnMore->show();
        x = sz.width();
        totalWidth += x;
    }
    else
    {
        btnMore->hide();
    }
    for( int i = count - 1; i >= 0; i-- )
    {
        if( totalWidth <= size.width() || i == 0)
        {
            buttons[i]->setGeometry( x, 0, qMin( size.width() - x, widths[i] ), size.height() );
            buttons[i]->show();
            x += widths[i];
            totalWidth -= widths[i];
        }
        else
        {
            menuMore->addAction( actions[i] );
            buttons[i]->hide();
            if( i < shown ) totalWidth -= widths[i];
        }
    }
}

void LocationBar::resizeEvent ( QResizeEvent * event )
{
    layOut( event->size() );
}

QSize LocationBar::sizeHint() const
{
    return btnMore->sizeHint();
}

LocationButton::LocationButton( const QString &text, bool bold,
                                bool arrow, QWidget * parent )
  : QPushButton( parent ), b_arrow( arrow )
{
    QFont font;
    font.setBold( bold );
    setFont( font );
    setText( text );
}

#define PADDING 4

void LocationButton::paintEvent ( QPaintEvent * )
{
    QStyleOptionButton option;
    option.initFrom( this );
    option.state |= QStyle::State_Enabled;
    QPainter p( this );

    if( underMouse() )
    {
        p.save();
        p.setRenderHint( QPainter::Antialiasing, true );
        QColor c = palette().color( QPalette::Highlight );
        p.setPen( c );
        p.setBrush( c.lighter( 150 ) );
        p.setOpacity( 0.2 );
        p.drawRoundedRect( option.rect.adjusted( 0, 2, 0, -2 ), 5, 5 );
        p.restore();
    }

    QRect r = option.rect.adjusted( PADDING, 0, -PADDING - (b_arrow ? 10 : 0), 0 );

    QString str( text() );
    /* This check is absurd, but either it is not done properly inside elidedText(),
       or boundingRect() is wrong */
    if( r.width() < fontMetrics().boundingRect( text() ).width() )
        str = fontMetrics().elidedText( text(), Qt::ElideRight, r.width() );
    p.drawText( r, Qt::AlignVCenter | Qt::AlignLeft, str );

    if( b_arrow )
    {
        option.rect.setWidth( 10 );
        option.rect.moveRight( rect().right() );
        style()->drawPrimitive( QStyle::PE_IndicatorArrowRight, &option, &p );
    }
}

QSize LocationButton::sizeHint() const
{
    QSize s( fontMetrics().boundingRect( text() ).size() );
    /* Add two pixels to width: font metrics are buggy, if you pass text through elidation
       with exactly the width of its bounding rect, sometimes it still elides */
    s.setWidth( s.width() + ( 2 * PADDING ) + ( b_arrow ? 10 : 0 ) + 2 );
    s.setHeight( s.height() + 2 * PADDING );
    return s;
}

#undef PADDING

#ifdef Q_OS_MAC
QSplitterHandle *PlaylistSplitter::createHandle()
{
    return new SplitterHandle( orientation(), this );
}

SplitterHandle::SplitterHandle( Qt::Orientation orientation, QSplitter * parent )
               : QSplitterHandle( orientation, parent)
{
};

QSize SplitterHandle::sizeHint() const
{
    return (orientation() == Qt::Horizontal) ? QSize( 1, height() ) : QSize( width(), 1 );
}

void SplitterHandle::paintEvent(QPaintEvent *event)
{
    QPainter painter( this );
    painter.fillRect( event->rect(), QColor(81, 81, 81) );
}
#endif /* __APPLE__ */
