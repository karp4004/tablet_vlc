import QtQuick 1.0
import "button"

Rectangle {
    id: top_widget
    objectName: "top_widget"
       
    signal state_changed(int state)
    
    //height: parent.height/6
    //width: parent.width
    color: Qt.rgba(0.,0.,0.,0.5)
     //opacity: 0.2
    state: 'visible'
    
 	property int height_prop: height
  
    gradient: Gradient {
        GradientStop { position: 0.0; color: Qt.rgba(0.,0.5,0.6,1.0) }
        GradientStop { position: 1.0; color: Qt.rgba(0.0,0.0,0.3,0.4) }
    } 
    
    property int button_field: width - width*0.1
    property int button_and_spacing: button_field*0.2
    property int button_size: button_and_spacing*0.2
    property int buton_spacing: button_and_spacing*0.8
    property int margin: (width - button_field + buton_spacing)/2
    
    Button {
    	id: settings_button
    	objectName: "settings_button"
    	
        x: top_widget.margin
        y: parent.height/2 - top_widget.button_size/2	    	
    	height: top_widget.button_size
    	width: height
    	
    	//s5, s6, s7, s8, s9
		normal_image: "../images/settings/s13.png"
		
		image_count: 1
		image_urls: ["../images/settings/s13.png"]
	}
	
	Button {
		id: fullscreen_button
    	objectName: "fullscreen_button"
		
		x: settings_button.x + top_widget.button_size + top_widget.buton_spacing
		y: parent.height/2 - top_widget.button_size/2
    	height: top_widget.button_size
    	width: height

		normal_image: "../images/fullscreen/f5.png"
		
		image_count: 2
		image_urls: ["../images/fullscreen/f7.png", "../images/fullscreen/f5.png"]
	}
	
	Button {
		id: playlist_button
    	objectName: "playlist_button"
		
		x: fullscreen_button.x + top_widget.button_size + top_widget.buton_spacing
		y: parent.height/2 - top_widget.button_size/2
    	height: top_widget.button_size
    	width: height
    		
		//p1 p9
		normal_image: "../images/playlist/p14.png"
		
		image_count: 1
		image_urls: ["../images/playlist/p14.png"]
	}	
	
	Button {
		id: loop_button
    	objectName: "loop_button"
		
		x: playlist_button.x + top_widget.button_size + top_widget.buton_spacing
		y: parent.height/2 - top_widget.button_size/2
    	height: top_widget.button_size
    	width: height
    	
		//2 3
		normal_image: "../images/loop/l3.png"
		
		image_count: 3
		image_urls: ["../images/loop/l9.png", "../images/loop/l10.png", "../images/loop/l7.png"]
	}	
	
	Button {
		id: random_button
    	objectName: "random_button"
		
		x: loop_button.x + top_widget.button_size + top_widget.buton_spacing
		y: parent.height/2 - top_widget.button_size/2
    	height: top_widget.button_size
    	width: height

		//r2
		normal_image: "../images/random/r2.png"
		
		image_count: 2
		image_urls: ["../images/random/r5.png", "../images/random/r4.png"]
	}	
	
	function switchState() {
         console.log(objectName, ":switchState:", state);//, " state: ", state);
         
         if(state == 'visible')
         {
         	state = 'invisible'
         }
         else
         {
         	state = 'visible'
         }
    }
    
    states: [
        State {
            name: "visible"
            PropertyChanges { target: top_widget; opacity: 1.0 }
        },

        State {
            name: "invisible"
            PropertyChanges { target: top_widget; opacity: 0.0 }
        }
    ]
    

    transitions: [

        Transition {
            from: "*"; to: "visible"
            NumberAnimation { properties: "opacity"; duration: 200 }
        },

        Transition {
            from: "*"; to: "invisible"
            NumberAnimation { properties: "opacity"; duration: 200 }
        },

        Transition {
            NumberAnimation { properties: "opacity"; duration: 200 }
        }
    ]
}
