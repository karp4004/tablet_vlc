/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.0
import "button"
import "progress"

Rectangle
{
	id: control_widget
	color: Qt.rgba(0.,0.,0.,0.0)
			
	property real spaceHeight: height - top_widget.height - bottom_widget.height
	property real spaceOffset: top_widget.height
	
	Rectangle {
	    id: top_widget
	    objectName: "top_widget"
	       
	    height: parent.height/6
	    width: parent.width
	    color: Qt.rgba(0.,0.,0.,0.5)
	     //opacity: 0.2
	    
	 	property int height_prop: height
	  
	    gradient: Gradient {
	        GradientStop { position: 0.0; color: Qt.rgba(0.,0.4,0.6,0.3) }
	        GradientStop { position: 1.0; color: Qt.rgba(0.,0.,0.0,0.0) }
	    } 
	    
	    property int button_field: width - width*0.1
	    property int button_and_spacing: button_field*0.2
	    property int button_size: button_and_spacing*0.2
	    property int buton_spacing: button_and_spacing*0.8
	    property int margin: (width - button_field + buton_spacing)/2
	    
	    Button {
	    	id: settings_button
	    	objectName: "settings_button"
	    	
	        x: top_widget.margin
	        y: parent.height/2 - top_widget.button_size/2	    	
	    	height: top_widget.button_size
	    	width: height
	    	
	    	//s5, s6, s7, s8, s9
    		normal_image: "../images/settings/s12.png"
		}
		
		Button {
			id: fullscreen_button
	    	objectName: "fullscreen_button"
			
			x: settings_button.x + top_widget.button_size + top_widget.buton_spacing
			y: parent.height/2 - top_widget.button_size/2
	    	height: top_widget.button_size
	    	width: height

    		normal_image: "../images/fullscreen/f3.png"
		}
		
		Button {
			id: playlist_button
	    	objectName: "playlist_button"
			
			x: fullscreen_button.x + top_widget.button_size + top_widget.buton_spacing
			y: parent.height/2 - top_widget.button_size/2
	    	height: top_widget.button_size
	    	width: height
	    		
			//p1 p9
    		normal_image: "../images/playlist/p1.png"
		}	
		
		Button {
			id: loop_button
	    	objectName: "loop_button"
			
			x: playlist_button.x + top_widget.button_size + top_widget.buton_spacing
			y: parent.height/2 - top_widget.button_size/2
	    	height: top_widget.button_size
	    	width: height
	    	
			//2 3
    		normal_image: "../images/loop/l3.png"
		}	
		
		Button {
			id: random_button
	    	objectName: "random_button"
			
			x: loop_button.x + top_widget.button_size + top_widget.buton_spacing
			y: parent.height/2 - top_widget.button_size/2
	    	height: top_widget.button_size
	    	width: height

			//r2
    		normal_image: "../images/random/r2.png"
		}	
	}
	
	Rectangle {
	    id: bottom_widget
	    objectName: "bottom_widget"
	    
	    y: parent.height/4*3
	    height: parent.height/4
	    width: parent.width
	    color: Qt.rgba(0.,0.,0.,0.5)
	     //opacity: 0.2
	     	 
	    property int y_prop: y
	     	   
	    property int button_field: width - width*0.4
	    property int button_and_spacing: button_field*0.2
	    property int button_size: button_and_spacing*0.6
	    property int buton_spacing: button_and_spacing*0.4
	    property int margin: (width - button_field + buton_spacing)/2
	     	    
	    gradient: Gradient {
	        GradientStop { position: 0.0; color: Qt.rgba(0.,0.,0.0,0.0) }
	        GradientStop { position: 1.0; color: Qt.rgba(0.,0.4,0.6,0.3) }
	    }
	    
	    Button {
	    	id: back_button
	    	objectName: "back_button"
	    	
	        x: bottom_widget.margin
			y: parent.height/2 - bottom_widget.button_size/2
	    	height: bottom_widget.button_size
	    	width: height

    		normal_image: "../images/controls/begin.png"
		}
		
		Button {
			id: backward_button
	    	objectName: "backward_button"
			
			x: back_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
			y: parent.height/2 - bottom_widget.button_size/2
	    	height: bottom_widget.button_size
	    	width: height

    		normal_image: "../images/controls/backward.png"
		}
		
		Button {
			id: play_button
	    	objectName: "play_button"
			
			x: backward_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
			y: parent.height/2 - bottom_widget.button_size/2
	    	height: bottom_widget.button_size
	    	width: height

    		normal_image: "../images/controls/play.png"
		}	
		
		Button {
			id: forward_button
	    	objectName: "forward_button"
			
			x: play_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
			y: parent.height/2 - bottom_widget.button_size/2
	    	height: bottom_widget.button_size
	    	width: height

    		normal_image: "../images/controls/forward.png"
		}	
		
		Button {
			id: end_button
	    	objectName: "end_button"
			
			x: forward_button.x + bottom_widget.button_size + bottom_widget.buton_spacing
			y: parent.height/2 - bottom_widget.button_size/2
	    	height: bottom_widget.button_size
	    	width: height

    		normal_image: "../images/controls/end.png"
		}	
		
		Button {
			id: record_button
	    	objectName: "record_button"
			
			x: bottom_widget.width - bottom_widget.margin/2
			y: parent.height/2 - bottom_widget.button_size/2 + height/2
	    	height: bottom_widget.button_size/2
	    	width: height

    		normal_image: "../images/controls5/record.png"
		}	
		
		Button {
	    	id: stop_button
	    	objectName: "stop_button"
	    	
	        x: record_button.x - bottom_widget.margin/4
	        y: parent.height/2 - bottom_widget.button_size/2 + height/2	    	
	    	height: bottom_widget.button_size/2
	    	width: height
	    	
	    	//s5, s6, s7, s8, s9
    		normal_image: "../images/controls5/stop.png"
		}
		
	}
	
	Progress {
		id: time_progress
	    objectName: "time_progress"
		
		property int offset:bottom_widget.width*0.05
		
		x: bottom_widget.x + offset
		y: bottom_widget.y*1.04
		width: bottom_widget.width - offset*2
		height: bottom_widget.height*0.05
		
		traveller_source: "../control_widget/images/sphere/s1.png"
	}
}