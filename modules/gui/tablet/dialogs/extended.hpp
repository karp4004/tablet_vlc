/*****************************************************************************
 * extended.hpp : Extended controls - Undocked
 ****************************************************************************
 * Copyright (C) 2006 the VideoLAN team
 * $Id: 3fd359651ad557ab8031034747f5331a14e01756 $
 *
 * Authors: Clément Stenac <zorglub@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef QVLC_EXTENDED_DIALOG_H_
#define QVLC_EXTENDED_DIALOG_H_ 1

#include <map>

#include "util/qvlcframe.hpp"

#include "components/extended_panels.hpp"
#include "util/singleton.hpp"

#include "ui/essential.h"
#include "ui/crop.h"
#include "ui/colors.h"
#include "ui/geomerty.h"
#include "ui/overlay.h"
#include "ui/atmo_light.h"
#include "ui/advanced.h"

using namespace std;

class QTabWidget;
class ExtendedDelegate;
class QListWidget;

class ExtendedDialog : public QVLCWidget, public Singleton<ExtendedDialog>
{
    Q_OBJECT
public:
    enum
    {
        AUDIO_TAB = 0,
        VIDEO_TAB,
        SYNCHRO_TAB,
        V4L2_TAB
    };
    void showTab( int i );
    int currentTab();

protected:
    void paintEvent(QPaintEvent * e);

private:
    ExtendedDialog( intf_thread_t * );
    virtual ~ExtendedDialog();

    void initComboBoxItems( QObject *widget );
    void setWidgetValue( QObject *widget );

    ExtendedDelegate* delegate;
    QListWidget* musicList;

    SyncControls *syncW;
    ExtVideo *videoEffect;
    Equalizer *equal;
    QTabWidget *mainTabW;
    QCheckBox *writeChangesBox;

    Ui::essential ui_essential;
    Ui::crop ui_crop;
    Ui::Colors ui_colors;
    Ui::geomerty ui_geomerty;
    Ui::overlay ui_overlay;
    Ui::atmo_light ui_atmo_light;
    Ui::advanced ui_advanced;

    map<QObject*, string> mOptions;
    map<QObject*, string> mModules;

private slots:
    void changedItem( int );
    void currentTabChanged( int );
	void updateFilters();
	void updateFilterOptions();

    friend class    Singleton<ExtendedDialog>;
};

#endif

