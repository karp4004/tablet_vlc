/*
 * extended_list.h
 *
 *  Created on: Dec 27, 2013
 *      Author: OlegKarpov
 */

#ifndef EXTENDED_LIST_H_
#define EXTENDED_LIST_H_

#include <QWidget>
#include <QImage>
#include <string>
#include <QBoxLayout>

using namespace std;

class QVBoxLayout;
class intf_thread_t;
class ExtendedList;
class QScrollArea;

class ExtendedListItem: public QWidget
{
	Q_OBJECT

public:
	enum Type
	{
		kEllipse,
		kRect
	};

	ExtendedListItem(intf_thread_t *_p_intf, string text, QFont font, string icon_path, QWidget* ud = 0, ExtendedList* parent=0, Type t=ExtendedListItem::kRect);
	int select();
	int unselect();
	int resizeFont(int sz);

	QWidget* userData;

protected:
	virtual void paintEvent(QPaintEvent *event);
	virtual void mousePressEvent( QMouseEvent * event );

private:
	int recursiveSetStyle(const QList<QObject*>& h_children, QFont& f);

	string mText;
	QImage mIcon;
	QFont mFont;
	bool mSelected;

	intf_thread_t *p_intf;
	Type mType;

signals:
	int onSelect(QObject* userdata);
};

class ExtendedList: public QWidget
{
	Q_OBJECT

public:
	ExtendedList(intf_thread_t *_p_intf, QBoxLayout::Direction itemDir, QBoxLayout::Direction panelDir, int ratio, QWidget* parent=0, unsigned char opacity = 0);
	int addItem(ExtendedListItem* item);
	int onItemSelect(ExtendedListItem* item);
	int addToMainLayout(QWidget* w, int spacing);
	int addToMainLayout(QBoxLayout* l, int spacing);

protected:
	virtual void resizeEvent(QResizeEvent * event);

private:
	QBoxLayout* mainLayout;
	QBoxLayout* mItemsLayout;
	QVBoxLayout* panelLayout;
	ExtendedListItem* currentPanel;
	QScrollArea *scroll;
	intf_thread_t *p_intf;
};

#endif /* EXTENDED_LIST_H_ */
