/*
 * extended_list.h
 *
 *  Created on: Dec 27, 2013
 *      Author: OlegKarpov
 */

#ifndef EXTENDED_TOOL_H_
#define EXTENDED_TOOL_H_

#include <QWidget>
#include <QFont>
#include <list>
#include <QToolButton>

using namespace std;

class QVBoxLayout;
class intf_thread_t;
class QResizeEvent;
class QToolBar;
class QToolButton;

class ExtendedToolButton: public QToolButton
{
public:
	ExtendedToolButton(QWidget* panel, QWidget* parent=0)
	:QToolButton(parent)
	,mPanel(panel)
	{
	}

	QWidget* mPanel;
};

class ExtendedTool: public QWidget
{
	Q_OBJECT

public:
	ExtendedTool(intf_thread_t *_p_intf, QWidget* parent =0);
	int addButton(QString text, QString iconPath, QWidget* panel, bool addSeparator);
	int switchTool(QObject* sender);

private:
	QVBoxLayout* audioPanelLayout;
    QWidget* currentAudioPanel;
    intf_thread_t *p_intf;
    QFont mFont;

    QToolBar* topBar;
    list<QAbstractButton*> mButtons;
    ExtendedToolButton* currentButton;

private slots:
	void onToolClick();
};

#endif /* EXTENDED_LIST_H_ */
