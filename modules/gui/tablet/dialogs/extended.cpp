/*****************************************************************************
 * extended.cpp : Extended controls - Undocked
 ****************************************************************************
 * Copyright (C) 2006-2008 the VideoLAN team
 * $Id: 0e9b906922250bbe08d52654b330e2a965df47bb $
 *
 * Authors: Clément Stenac <zorglub@videolan.org>
 *          Jean-Baptiste Kempf <jb@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#define __STDC_FORMAT_MACROS 1

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <math.h>

#include "dialogs/extended.hpp"
#include "dialogs/extended_list.hpp"
#include "dialogs/extended_tool.hpp"

#include "main_interface.hpp" /* Show playlist */
#include "input_manager.hpp"

#include <QTabWidget>
#include <QGridLayout>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QListWidget>
#include <QItemDelegate>
#include <QPainter>
#include <vlc_modules.h>
#include <vlc_vout.h>
#include <vlc_plugin.h>
#include <src/libvlc.h>
#include <src/misc/variables.h>

ExtendedDialog::ExtendedDialog( intf_thread_t *_p_intf )
: QVLCWidget( _p_intf )
{
#ifdef __APPLE__
    setWindowFlags( Qt::Drawer );
#else
    setWindowFlags( Qt::Tool );
#endif

    setWindowOpacity( var_InheritFloat( p_intf, "qt-opacity" ) );
    setWindowTitle( qtr( "Adjustments and Effects" ) );
    setWindowRole( "vlc-extended" );

	QFont font;

    ExtendedList* lst = new ExtendedList(_p_intf, QBoxLayout::TopToBottom, QBoxLayout::LeftToRight, 3, this);

    ExtendedList* audioTool = new ExtendedList(_p_intf, QBoxLayout::LeftToRight, QBoxLayout::TopToBottom, 5, this, 100);
	Equalizer* eq = new Equalizer( p_intf, this );
	Compressor *comp = new Compressor( p_intf, this );
	Spatializer *spat = new Spatializer( p_intf, this );
	ExtendedListItem* at1 = new ExtendedListItem(_p_intf, "Equalizer", font, "/usr/local/share/qml/images/apple/a1.png", eq, audioTool);
	ExtendedListItem* at2 = new ExtendedListItem(_p_intf, "Compressor", font, "/usr/local/share/qml/images/apple/a1.png", comp, audioTool);
	ExtendedListItem* at3 = new ExtendedListItem(_p_intf, "Spatializer", font, "/usr/local/share/qml/images/apple/a1.png", spat, audioTool);

	ExtendedList* videoTool = new ExtendedList(_p_intf, QBoxLayout::LeftToRight, QBoxLayout::TopToBottom, 5, this, 100);

	QWidget* Essential = new QWidget(  this );
	QWidget *Crop = new QWidget(  this );
	QWidget *Colors = new QWidget(  this );
	QWidget *Geomerty = new QWidget(  this );
	QWidget *Overlay = new QWidget(  this );
	QWidget *AtmoLight = new QWidget(  this );
	QWidget *Advanced = new QWidget(  this );

    ui_essential.setupUi( Essential );
    ui_crop.setupUi( Crop );
    ui_colors.setupUi( Colors );
    ui_geomerty.setupUi( Geomerty );
    ui_overlay.setupUi( Overlay );
    ui_atmo_light.setupUi( AtmoLight );
    ui_advanced.setupUi( Advanced );

	ExtendedListItem* vt1 = new ExtendedListItem(_p_intf, "Essential", font, "/usr/local/share/qml/images/apple/a1.png", Essential, videoTool);
	ExtendedListItem* vt2 = new ExtendedListItem(_p_intf, "Crop", font, "/usr/local/share/qml/images/apple/a1.png", Crop, videoTool);
	ExtendedListItem* vt3 = new ExtendedListItem(_p_intf, "Colors", font, "/usr/local/share/qml/images/apple/a1.png", Colors, videoTool);
	ExtendedListItem* vt4 = new ExtendedListItem(_p_intf, "Geomerty", font, "/usr/local/share/qml/images/apple/a1.png", Geomerty, videoTool);
	ExtendedListItem* vt5 = new ExtendedListItem(_p_intf, "Overlay", font, "/usr/local/share/qml/images/apple/a1.png", Overlay, videoTool);
	ExtendedListItem* vt6 = new ExtendedListItem(_p_intf, "AtmoLight", font, "/usr/local/share/qml/images/apple/a1.png", AtmoLight, videoTool);
	ExtendedListItem* vt7 = new ExtendedListItem(_p_intf, "Advanced", font, "/usr/local/share/qml/images/apple/a1.png", Advanced, videoTool);

//	ExtVideo* videoEffect = new ExtVideo( p_intf, this );
//	audioSettings->addButton("Video Effect", ":/images/clock_alert.png", videoEffect, true);

	SyncControls* syn = new SyncControls( p_intf, this );
    ExtV4l2 *v4l2panel = new ExtV4l2( p_intf, this );

	ExtendedListItem* i1 = new ExtendedListItem(_p_intf, "Audio Effects", font, "/usr/local/share/qml/images/apple/a1.png", audioTool, lst);
	ExtendedListItem* i2 = new ExtendedListItem(_p_intf, "Video Effects", font, "/usr/local/share/qml/images/apple/a1.png", videoTool, lst);
	ExtendedListItem* i3 = new ExtendedListItem(_p_intf, "Synchronization", font, "/usr/local/share/qml/images/apple/a1.png", syn, lst);
	ExtendedListItem* i4 = new ExtendedListItem(_p_intf, "v4l2 controls", font, "/usr/local/share/qml/images/apple/a1.png", v4l2panel, lst);

	QHBoxLayout* l = new QHBoxLayout(this);
	l->addWidget(lst);
	//l->addWidget(w,3);
	l->setSpacing(0);
	l->setMargin(0);

	setLayout(l);

#define SETUP_VFILTER( widget, filter ) \
    { \
        vlc_object_t *p_obj = ( vlc_object_t * ) \
            vlc_object_find_name( p_intf->p_libvlc, \
                                  #filter ); \
        QCheckBox *checkbox = qobject_cast<QCheckBox*>( widget ); \
        QGroupBox *groupbox = qobject_cast<QGroupBox*>( widget ); \
        if( p_obj ) \
        { \
            vlc_object_release( p_obj ); \
            if( checkbox ) checkbox->setChecked( true ); \
            else if (groupbox) groupbox->setChecked( true ); \
        } \
        else \
        { \
            if( checkbox ) checkbox->setChecked( false ); \
            else if (groupbox)  groupbox->setChecked( false ); \
        } \
    } \
    CONNECT( widget, clicked(), this, updateFilters() );


#define SETUP_VFILTER_OPTION( widget, signal ) \
    initComboBoxItems( widget ); \
    setWidgetValue( widget ); \
    CONNECT( widget, signal, this, updateFilterOptions() );

    SETUP_VFILTER( ui_essential.adjustEnable, adjust )
    SETUP_VFILTER_OPTION( ui_essential.hueSlider_2, valueChanged( int ) )
    SETUP_VFILTER_OPTION( ui_essential.contrastSlider_2, valueChanged( int ) )
    SETUP_VFILTER_OPTION( ui_essential.brightnessSlider_2, valueChanged( int ) )
    SETUP_VFILTER_OPTION( ui_essential.saturationSlider_2, valueChanged( int ) )
    SETUP_VFILTER_OPTION( ui_essential.gammaSlider_2, valueChanged( int ) )

    mModules[ui_essential.adjustEnable] = "adjust";
	mOptions[ui_essential.hueSlider_2] = "hue";
	mModules[ui_essential.hueSlider_2] = "adjust";
	mOptions[ui_essential.contrastSlider_2] = "contrast";
	mModules[ui_essential.contrastSlider_2] = "adjust";
	mOptions[ui_essential.brightnessSlider_2] = "brightness";
	mModules[ui_essential.brightnessSlider_2] = "adjust";
	mOptions[ui_essential.saturationSlider_2] = "saturation";
	mModules[ui_essential.saturationSlider_2] = "adjust";
	mOptions[ui_essential.gammaSlider_2] = "gamma";
	mModules[ui_essential.gammaSlider_2] = "adjust";

	return;

    QVBoxLayout *layout = new QVBoxLayout( this );
    layout->setContentsMargins( 0, 2, 0, 1 );
    layout->setSpacing( 3 );

    mainTabW = new QTabWidget( this );

    /* AUDIO effects */
    QWidget *audioWidget = new QWidget;
    QHBoxLayout *audioLayout = new QHBoxLayout( audioWidget );
    QTabWidget *audioTab = new QTabWidget( audioWidget );

    equal = new Equalizer( p_intf, audioTab );
    audioTab->addTab( equal, qtr( "Graphic Equalizer" ) );

    Compressor *compres = new Compressor( p_intf, audioTab );
    audioTab->addTab( compres, qtr( "Compressor" ) );

    Spatializer *spatial = new Spatializer( p_intf, audioTab );
    audioTab->addTab( spatial, qtr( "Spatializer" ) );
    audioLayout->addWidget( audioTab );

    mainTabW->insertTab( AUDIO_TAB, audioWidget, qtr( "Audio Effects" ) );

    /* Video Effects */
    QWidget *videoWidget = new QWidget;
    QHBoxLayout *videoLayout = new QHBoxLayout( videoWidget );
    QTabWidget *videoTab = new QTabWidget( videoWidget );

    videoEffect = new ExtVideo( p_intf, videoTab );
    videoLayout->addWidget( videoTab );
    videoTab->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Maximum );

    mainTabW->insertTab( VIDEO_TAB, videoWidget, qtr( "Video Effects" ) );

    syncW = new SyncControls( p_intf, videoTab );
    mainTabW->insertTab( SYNCHRO_TAB, syncW, qtr( "Synchronization" ) );

    if( module_exists( "v4l2" ) )
    {
        ExtV4l2 *v4l2 = new ExtV4l2( p_intf, mainTabW );
        mainTabW->insertTab( V4L2_TAB, v4l2, qtr( "v4l2 controls" ) );
    }

    layout->addWidget( mainTabW );

    /* Bottom buttons / checkbox line */
    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    layout->addLayout( buttonsLayout );

    writeChangesBox = new QCheckBox( qtr("&Write changes to config") );
    buttonsLayout->addWidget( writeChangesBox );
    CONNECT( writeChangesBox, toggled(bool), compres, setSaveToConfig(bool) );
    CONNECT( writeChangesBox, toggled(bool), spatial, setSaveToConfig(bool) );
    CONNECT( writeChangesBox, toggled(bool), equal, setSaveToConfig(bool) );
    CONNECT( mainTabW, currentChanged(int), this, currentTabChanged(int) );

    QDialogButtonBox *closeButtonBox = new QDialogButtonBox( Qt::Horizontal, this );
    closeButtonBox->addButton(
        new QPushButton( qtr("&Close"), this ), QDialogButtonBox::RejectRole );
    buttonsLayout->addWidget( closeButtonBox );

    CONNECT( closeButtonBox, rejected(), this, close() );

    /* Restore geometry or move this dialog on the left pane of the MI */
    if( !restoreGeometry( getSettings()->value("EPanel/geometry").toByteArray() ) )
    {
        resize( QSize( 400, 280 ) );

        MainInterface *p_mi = p_intf->p_sys->p_mi;
        if( p_mi && p_mi->x() > 50 )
            move( ( p_mi->x() - frameGeometry().width() - 10 ), p_mi->y() );
        else
            move ( 450 , 0 );
    }

    CONNECT( THEMIM->getIM(), playingStatusChanged( int ), this, changedItem( int ) );
}

ExtendedDialog::~ExtendedDialog()
{
    getSettings()->setValue("Epanel/geometry", saveGeometry());
}

void ExtendedDialog::setWidgetValue( QObject *widget )
{
    QString module;
    QString option;

    map<QObject*, string>::iterator mModules_it = mModules.find(sender());
    if(mModules_it != mModules.end())
    {
		module = mModules_it->second.c_str();
    }
    else
    {
        fprintf(stderr, "%s%s:no module\n", __FILE__, __FUNCTION__);
        return;
    }

    map<QObject*, string>::iterator mOptions_it = mOptions.find(sender());
    if(mOptions_it != mOptions.end())
    {
		option = mOptions_it->second.c_str();
    }
    else
    {
        fprintf(stderr, "%s%s:no option\n", __FILE__, __FUNCTION__);
        return;
    }

    fprintf(stderr, "%s%s:module:%s\n", __FILE__, __FUNCTION__, module.toStdString().c_str());
    fprintf(stderr, "%s%s:option:%s\n", __FILE__, __FUNCTION__, option.toStdString().c_str());
    fprintf(stderr, "%s%s:p_intf:%s\n", __FILE__, __FUNCTION__, p_intf);
    fprintf(stderr, "%s%s:p_sys:%s\n", __FILE__, __FUNCTION__, p_intf->p_sys);
    fprintf(stderr, "%s%s:p_mi:%s\n", __FILE__, __FUNCTION__, p_intf->p_sys->p_mi);

    vlc_object_t *p_obj = ( vlc_object_t * )
        vlc_object_find_name( p_intf->p_sys->p_mi->getInterface(), qtu( module ) );
    int i_type;
    vlc_value_t val;

    if( !p_obj )
    {
#if 0
        msg_Dbg( p_intf,
                 "Module instance %s not found, looking in config values.",
                 qtu( module ) );
#endif
        i_type = config_GetType( p_intf, qtu( option ) ) & VLC_VAR_CLASS;
        switch( i_type )
        {
            case VLC_VAR_INTEGER:
            case VLC_VAR_BOOL:
                val.i_int = config_GetInt( p_intf, qtu( option ) );
                break;
            case VLC_VAR_FLOAT:
                val.f_float = config_GetFloat( p_intf, qtu( option ) );
                break;
            case VLC_VAR_STRING:
                val.psz_string = config_GetPsz( p_intf, qtu( option ) );
                break;
        }
    }
    else
    {
        i_type = var_Type( p_obj, qtu( option ) ) & VLC_VAR_CLASS;
        var_Get( p_obj, qtu( option ), &val );
        vlc_object_release( p_obj );
    }

    /* Try to cast to all the widgets we're likely to encounter. Only
     * one of the casts is expected to work. */
    QSlider        *slider        = qobject_cast<QSlider*>       ( widget );
    QCheckBox      *checkbox      = qobject_cast<QCheckBox*>     ( widget );
    QSpinBox       *spinbox       = qobject_cast<QSpinBox*>      ( widget );
    QDoubleSpinBox *doublespinbox = qobject_cast<QDoubleSpinBox*>( widget );
    QDial          *dial          = qobject_cast<QDial*>         ( widget );
    QLineEdit      *lineedit      = qobject_cast<QLineEdit*>     ( widget );
    QComboBox      *combobox      = qobject_cast<QComboBox*>     ( widget );

    if( i_type == VLC_VAR_INTEGER || i_type == VLC_VAR_BOOL )
    {
        if( slider )        slider->setValue( val.i_int );
        else if( checkbox ) checkbox->setCheckState( val.i_int? Qt::Checked
                                                              : Qt::Unchecked );
        else if( spinbox )  spinbox->setValue( val.i_int );
        else if( dial )     dial->setValue( ( 540-val.i_int )%360 );
        else if( lineedit )
        {
            char str[30];
            snprintf( str, sizeof(str), "%06"PRIX64, val.i_int );
            lineedit->setText( str );
        }
        else if( combobox ) combobox->setCurrentIndex(
                            combobox->findData( qlonglong(val.i_int) ) );
        else msg_Warn( p_intf, "Could not find the correct Integer widget" );
    }
    else if( i_type == VLC_VAR_FLOAT )
    {
        if( slider ) slider->setValue( ( int )( val.f_float*( double )slider->tickInterval() ) ); /* hack alert! */
        else if( doublespinbox ) doublespinbox->setValue( val.f_float );
        else if( dial ) dial->setValue( (540 - lroundf(val.f_float)) % 360 );
        else msg_Warn( p_intf, "Could not find the correct Float widget" );
    }
    else if( i_type == VLC_VAR_STRING )
    {
        if( lineedit ) lineedit->setText( qfu( val.psz_string ) );
        else if( combobox ) combobox->setCurrentIndex(
                            combobox->findData( qfu( val.psz_string ) ) );
        else msg_Warn( p_intf, "Could not find the correct String widget" );
        free( val.psz_string );
    }
    else
        if( p_obj )
            msg_Err( p_intf,
                     "Module %s's %s variable is of an unsupported type ( %d )",
                     qtu( module ),
                     qtu( option ),
                     i_type );
}

void ExtendedDialog::initComboBoxItems( QObject *widget )
{
    QComboBox *combobox = qobject_cast<QComboBox*>( widget );
    if( !combobox ) return;

    QString option;

    map<QObject*, string>::iterator mOptions_it = mOptions.find(widget);
    if(mOptions_it != mOptions.end())
    {
		option = mOptions_it->second.c_str();
    }
    else
    {
        fprintf(stderr, "%s%s:no option\n", __FILE__, __FUNCTION__);
        return;
    }

    module_config_t *p_item = config_FindConfig( VLC_OBJECT( p_intf ),
                                                 qtu( option ) );
    if( p_item == NULL )
    {
        msg_Err( p_intf, "Couldn't find option \"%s\".", qtu( option ) );
        return;
    }

    if( p_item->i_type == CONFIG_ITEM_INTEGER
     || p_item->i_type == CONFIG_ITEM_BOOL )
    {
        int64_t *values;
        char **texts;
        ssize_t count = config_GetIntChoices( VLC_OBJECT( p_intf ),
                                              qtu( option ), &values, &texts );
        for( ssize_t i = 0; i < count; i++ )
        {
            combobox->addItem( qtr( texts[i] ), qlonglong(values[i]) );
            free( texts[i] );
        }
        free( texts );
        free( values );
    }
    else if( p_item->i_type == CONFIG_ITEM_STRING )
    {
        char **values;
        char **texts;
        ssize_t count = config_GetPszChoices( VLC_OBJECT( p_intf ),
                                              qtu( option ), &values, &texts );
        for( ssize_t i = 0; i < count; i++ )
        {
            combobox->addItem( qtr( texts[i] ), qfu(values[i]) );
            free( texts[i] );
            free( values[i] );
        }
        free( texts );
        free( values );
    }
}

static void ChangeVFiltersString( struct intf_thread_t *p_intf, const char *psz_name, bool b_add )
{
    char *psz_parser, *psz_string;
    const char *psz_filter_type;

    module_t *p_obj = module_find( psz_name );
    if( !p_obj )
    {
        msg_Err( p_intf, "Unable to find filter module \"%s\".", psz_name );
        return;
    }

    if( module_provides( p_obj, "video splitter" ) )
    {
        psz_filter_type = "video-splitter";
    }
    else if( module_provides( p_obj, "video filter2" ) )
    {
        psz_filter_type = "video-filter";
    }
    else if( module_provides( p_obj, "sub source" ) )
    {
        psz_filter_type = "sub-source";
    }
    else if( module_provides( p_obj, "sub filter" ) )
    {
        psz_filter_type = "sub-filter";
    }
    else
    {
        msg_Err( p_intf, "Unknown video filter type." );
        return;
    }

    psz_string = config_GetPsz( p_intf, psz_filter_type );

    if( !psz_string ) psz_string = strdup( "" );

    psz_parser = strstr( psz_string, psz_name );

    if( b_add )
    {
        if( !psz_parser )
        {
            psz_parser = psz_string;
            if( asprintf( &psz_string, ( *psz_string ) ? "%s:%s" : "%s%s",
                            psz_string, psz_name ) == -1 )
            {
                free( psz_parser );
                return;
            }
            free( psz_parser );
        }
        else
        {
            free( psz_string );
            return;
        }
    }
    else
    {
        if( psz_parser )
        {
            if( *( psz_parser + strlen( psz_name ) ) == ':' )
            {
                memmove( psz_parser, psz_parser + strlen( psz_name ) + 1,
                         strlen( psz_parser + strlen( psz_name ) + 1 ) + 1 );
            }
            else
            {
                *psz_parser = '\0';
            }

            /* Remove trailing : : */
            size_t i_len = strlen( psz_string );
            if( i_len > 0 && *( psz_string + i_len - 1 ) == ':' )
            {
                *( psz_string + i_len - 1 ) = '\0';
            }
        }
        else
        {
            free( psz_string );
            return;
        }
    }
    /* Vout is not kept, so put that in the config */
    config_PutPsz( p_intf, psz_filter_type, psz_string );

    /* Try to set on the fly */
    if( !strcmp( psz_filter_type, "video-splitter" ) )
    {
        playlist_t *p_playlist = pl_Get( p_intf );
        var_SetString( p_playlist, psz_filter_type, psz_string );
    }
    else
    {
        vout_thread_t *p_vout = THEMIM->getVout();
        if( p_vout )
        {
            var_SetString( p_vout, psz_filter_type, psz_string );
            vlc_object_release( p_vout );
        }
    }

    free( psz_string );
}

void ExtendedDialog::updateFilters()
{
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);

	map<QObject*, string>::iterator mModules_it = mModules.find(sender());
    if(mModules_it != mModules.end())
    {
		QString module = mModules_it->second.c_str();

    	fprintf(stderr, "%s:%s:module:%s\n", __FILE__, __FUNCTION__, module.toStdString().c_str());

		QCheckBox *checkbox = qobject_cast<QCheckBox*>( sender() );
		QGroupBox *groupbox = qobject_cast<QGroupBox*>( sender() );

		ChangeVFiltersString( p_intf, qtu( module ),
							  checkbox ? checkbox->isChecked()
									   : groupbox->isChecked() );
    }
    else
    {
    	fprintf(stderr, "%s:%s:no module\n", __FILE__, __FUNCTION__);
    }
}

void ExtendedDialog::updateFilterOptions()
{
    QString module;
    QString option;

    map<QObject*, string>::iterator mModules_it = mModules.find(sender());
    if(mModules_it != mModules.end())
    {
		module = mModules_it->second.c_str();
    }
    else
    {
        fprintf(stderr, "%s%s:no module\n", __FILE__, __FUNCTION__);
        return;
    }

    map<QObject*, string>::iterator mOptions_it = mOptions.find(sender());
    if(mOptions_it != mOptions.end())
    {
		option = mOptions_it->second.c_str();
    }
    else
    {
        fprintf(stderr, "%s%s:no option\n", __FILE__, __FUNCTION__);
        return;
    }

    fprintf(stderr, "%s:%s:module:%s\n", __FILE__, __FUNCTION__, module.toStdString().c_str());
    fprintf(stderr, "%s:%s:option:%s\n", __FILE__, __FUNCTION__, option.toStdString().c_str());
    fprintf(stderr, "%s:%s:p_intf:%d\n", __FILE__, __FUNCTION__, p_intf);
    fprintf(stderr, "%s:%s:p_sys:%d\n", __FILE__, __FUNCTION__, p_intf->p_sys);
    fprintf(stderr, "%s:%s:p_mi:%d\n", __FILE__, __FUNCTION__, p_intf->p_sys->p_mi);

    libvlc_int_t* int_t = p_intf->p_sys->p_mi->getInterface();

	fprintf(stderr, "%s:%s:%d:int_t:%d\n", __FILE__, __FUNCTION__, __LINE__, int_t);
	fprintf(stderr, "%s:%s:%d:p_libvlc:%d\n", __FILE__, __FUNCTION__, __LINE__, int_t->p_libvlc);

    vlc_object_t *p_obj = ( vlc_object_t * ) vlc_object_find_name( VLC_OBJECT(int_t), qtu( module ) );

    fprintf(stderr, "%s%s:p_obj:%d\n", __FILE__, __FUNCTION__, p_obj);

    int i_type;
    bool b_is_command;
    if( !p_obj )
    {
    	fprintf(stderr, "Module %s not found. You'll need to restart the filter to take the change into account.", module.toStdString().c_str() );
        i_type = config_GetType( p_intf, qtu( option ) );
        b_is_command = false;
    }
    else
    {
        fprintf(stderr, "%s%s:psz_object_type:%s\n", __FILE__, __FUNCTION__, p_obj->psz_object_type);

        i_type = var_Type( p_obj, qtu( option ) );
        if( i_type == 0 )
            i_type = config_GetType( p_intf, qtu( option ) );
        b_is_command = ( i_type & VLC_VAR_ISCOMMAND );
    }

    /* Try to cast to all the widgets we're likely to encounter. Only
     * one of the casts is expected to work. */
    QSlider        *slider        = qobject_cast<QSlider*>       ( sender() );
    QCheckBox      *checkbox      = qobject_cast<QCheckBox*>     ( sender() );
    QSpinBox       *spinbox       = qobject_cast<QSpinBox*>      ( sender() );
    QDoubleSpinBox *doublespinbox = qobject_cast<QDoubleSpinBox*>( sender() );
    QDial          *dial          = qobject_cast<QDial*>         ( sender() );
    QLineEdit      *lineedit      = qobject_cast<QLineEdit*>     ( sender() );
    QComboBox      *combobox      = qobject_cast<QComboBox*>     ( sender() );

    i_type &= VLC_VAR_CLASS;
    if( i_type == VLC_VAR_INTEGER || i_type == VLC_VAR_BOOL )
    {
        int i_int = 0;
        if( slider )        i_int = slider->value();
        else if( checkbox ) i_int = checkbox->checkState() == Qt::Checked;
        else if( spinbox )  i_int = spinbox->value();
        else if( dial )     i_int = ( 540-dial->value() )%360;
        else if( lineedit ) i_int = lineedit->text().toInt( NULL,16 );
        else if( combobox ) i_int = combobox->itemData( combobox->currentIndex() ).toInt();
        else fprintf(stderr, "Could not find the correct Integer widget" );
        config_PutInt( p_intf, qtu( option ), i_int );
        if( b_is_command )
        {
            if( i_type == VLC_VAR_INTEGER )
                var_SetInteger( p_obj, qtu( option ), i_int );
            else
                var_SetBool( p_obj, qtu( option ), i_int );
        }
    }
    else if( i_type == VLC_VAR_FLOAT )
    {
        double f_float = 0;
        if( slider )             f_float = ( double )slider->value()
                                         / ( double )slider->tickInterval(); /* hack alert! */
        else if( doublespinbox ) f_float = doublespinbox->value();
        else if( dial ) f_float = (540 - dial->value()) % 360;
        else if( lineedit ) f_float = lineedit->text().toDouble();
        else fprintf(stderr, "Could not find the correct Float widget" );
        config_PutFloat( p_intf, qtu( option ), f_float );
        if( b_is_command )
            var_SetFloat( p_obj, qtu( option ), f_float );
    }
    else if( i_type == VLC_VAR_STRING )
    {
        QString val;
        if( lineedit )
            val = lineedit->text();
        else if( combobox )
            val = combobox->itemData( combobox->currentIndex() ).toString();
        else
        	fprintf(stderr, "Could not find the correct String widget" );
        config_PutPsz( p_intf, qtu( option ), qtu( val ) );
        if( b_is_command )
            var_SetString( p_obj, qtu( option ), qtu( val ) );
    }
    else
    	fprintf(stderr,
                 "Module %s's %s variable is of an unsupported type ( %d )",
                 module.toStdString().c_str(),
                 option.toStdString().c_str(),
                 i_type );

    if( !b_is_command )
    {
    	fprintf(stderr, "Module %s's %s variable isn't a command. Brute-restarting the filter.",
                 module.toStdString().c_str(),
                 option.toStdString().c_str() );
        ChangeVFiltersString( p_intf, qtu( module ), false );
        ChangeVFiltersString( p_intf, qtu( module ), true );
    }

    if( p_obj ) vlc_object_release( p_obj );
}

void ExtendedDialog::showTab( int i )
{
	if(mainTabW)
	{
		mainTabW->setCurrentIndex( i );
	}

    show();
}

int ExtendedDialog::currentTab()
{
	int index = 0;
	if(mainTabW)
	{
		index = mainTabW->currentIndex();
	}

    return index;
}

void ExtendedDialog::changedItem( int i_status )
{
    if( i_status != END_S ) return;

    if(syncW)
    {
    	syncW->clean();
    }

    if(videoEffect)
    {
    	videoEffect->clean();
    }
}

void ExtendedDialog::currentTabChanged( int i )
{
	if(writeChangesBox)
	{
		writeChangesBox->setVisible( i == AUDIO_TAB );
	}
}

void ExtendedDialog::paintEvent(QPaintEvent * e)
{
	QVLCWidget::paintEvent(e);
}
