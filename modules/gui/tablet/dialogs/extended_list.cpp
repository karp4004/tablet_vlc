/*
 * extended_list.cpp
 *
 *  Created on: Dec 27, 2013
 *      Author: OlegKarpov
 */
#include "extended_list.hpp"

#include "stdio.h"

#include "tablet.hpp"
#include "vlc_interface.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPainter>
#include <QScrollArea>
#include <QGroupBox>

#define EXTENDED_LIST_TRACE 0
#define EXTENDED_PAINT 0
#define LIST_DEBUG 0
#define LIST_DEBUG_NONTRANSPARENT 0

ExtendedListItem::ExtendedListItem(intf_thread_t *_p_intf, string text, QFont font, string icon_path, QWidget* ud, ExtendedList* parent, Type t)
:QWidget(parent)
,mText(text)
,mFont(font)
,mIcon(icon_path.c_str())
,userData(ud)
,mSelected(false)
,p_intf(_p_intf)
,mType(t)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	QWidget* uw = (QWidget*)ud;
	if(uw)
	{
		int i_font_size = var_InheritInteger( p_intf, "qt-font-size" );
		fprintf(stderr, "%s:%s:%d:i_font_size:%d\n", __FILE__, __FUNCTION__, __LINE__, i_font_size);
		font.setPointSize(20);

		uw->setFont(font);

		uw->hide();
	}

	setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);

	parent->addItem(this);
}

int ExtendedListItem::recursiveSetStyle(const QList<QObject*>& h_children, QFont& f)
{
#if LIST_DEBUG
	fprintf(stderr, "%s:%s:%d:%d\n", __FILE__, __FUNCTION__, __LINE__, h_children.size());
#endif

	for(int i=0;i<h_children.size();i++)
	 {
#if LIST_DEBUG
		fprintf(stderr, "%s:%s:%d:type:%s\n", __FILE__, __FUNCTION__, __LINE__, h_children.at(i)->metaObject()->className());
#endif

		 QObject* o = h_children.at(i);
		 if(o->isWidgetType())
		 {
#if LIST_DEBUG
			 fprintf(stderr, "%s:%s:%d:isWidgetType\n", __FILE__, __FUNCTION__, __LINE__);
#endif

			 QWidget* w = (QWidget*)o;
			 w->setFont(f);

			 std::string cn = w->metaObject()->className();
			 if(cn == "QComboBox" || cn == "QSpinBox" || cn == "QLineEdit"  || cn == "QDoubleSpinBox"  || cn == "QPushButton")
			 {
#if LIST_DEBUG_NONTRANSPARENT
				 fprintf(stderr, "%s:%s:%d:type:%s:parent:%s\n", __FILE__, __FUNCTION__, __LINE__, h_children.at(i)->metaObject()->className(), w->parentWidget()->metaObject()->className());
#endif
				 //fprintf(stderr, "%s:%s:%d:palette:%s\n", __FILE__, __FUNCTION__, __LINE__, cn.c_str());
				 w->setStyleSheet(//"QLineEdit {"
     "background: qradialgradient(cx:0, cy:0, radius: 1,"
                "fx:1.0, fy:1.0, stop:0 rgba(255,255,255, 150), stop:1 rgba(255,255,255, 250));"
     "border-radius: 4px;"
	"border-style: outset;"
	"border-width: 2px;"
	"border-color: rgba(100,100,255, 150);"
    //"}"
);

//				QPalette palette = w->palette();
//				palette.setColor(QPalette::Base, Qt::yellow);
//				palette.setColor(QPalette::Background, Qt::yellow);
//				palette.setColor(QPalette::Window, Qt::yellow);
//				palette.setColor(QPalette::WindowText, Qt::yellow);
//				palette.setColor(QPalette::Foreground, Qt::yellow);
//				w->setPalette(palette);
			 }
		 }

		 recursiveSetStyle(o->children(), f);
	 }

	 return 0;
}

void ExtendedListItem::paintEvent(QPaintEvent *event)
{
#if EXTENDED_PAINT
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	float line_bottom = (float)height()*0.99;
	float line_right = (float)width()*0.99;
	float fs = (width()+height())/15;

	QPainter painter(this);

	QColor col(QColor(0,0,0,0));
	if(mSelected)
	{
		col = QColor(200,200,255,250);
	}

	QLinearGradient linearGrad(QPointF(0, 0), QPointF(width(), height()));
	linearGrad.setColorAt(0, QColor(0,0,0,200));
	linearGrad.setColorAt(1, col);

	QBrush br(linearGrad);


	painter.setBrush(br);
	painter.setPen ( QColor(255, 255, 255) );

	const QPointF points[5] = {
	    QPointF(0.0, 0.0),
	    QPointF(width()*0.1, height()),
	    QPointF(width()*0.9, height()),
	    QPointF(width(), 0),
	    QPointF(0, 0),
	};

	switch(mType)
	{
	case kEllipse:
		painter.drawPolygon(points, 5);
		break;

	case kRect:
		painter.drawRoundedRect(0,0,line_right, line_bottom, fs, fs);
//	    painter.drawLine( 0, line_bottom, line_right, line_bottom );
//	    painter.drawLine( line_right, 0, line_right, line_bottom );
		break;

	default:
		break;
	}

    if(!mIcon.isNull())
	{
    	painter.drawImage(QPoint(0,0), mIcon.scaled(size()));
	}


	QFont f = font();
	f.setPointSize(fs);
	painter.setFont(f);

	painter.drawText(width()*0.2, height()*0.2, width(), height(), 0, mText.c_str());
}

void ExtendedListItem::mousePressEvent(QMouseEvent * event)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	ExtendedList* p = (ExtendedList*)parentWidget();
	p->onItemSelect(this);
}

int ExtendedListItem::select()
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	mSelected = true;
	repaint();

	emit onSelect(userData);

	return 0;
}

int ExtendedListItem::unselect()
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	mSelected = false;
	repaint();

	return 0;
}

int ExtendedListItem::resizeFont(int sz)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	fprintf(stderr, "%s:%s:%d:size:%d\n", __FILE__, __FUNCTION__, __LINE__, sz);
	fprintf(stderr, "%s:%s:%d:class-------------:%s\n", __FILE__, __FUNCTION__, __LINE__, userData->metaObject()->className());

	if(userData)
	{
		char pattern[1024];

		sprintf(pattern, "QToolButton, QWidget, QComboBox, QLineEdit, QCheckBox,"
				"QSpinBox, QPushButton, QDoubleSpinBox, QTimeEdit, QListWidget, QToolButton, QRadioButton{"
				"background: rgba(200,200,255,0);"//"background: qradialgradient(cx:0, cy:0, radius: 2,"
				//	   "fx:1.0, fy:1.0, stop:0 rgba(0,0,0, 0), stop:1 rgba(200,255,200,250));"
		"border-radius: 8px;"
		"border-style: outset;"
		"border-width: 1px;"
		"border-color: rgba(200,255,200,150);"
		"color: white;"
		"font: bold %dpx;"
		"}", sz);

		char pattern2[256];
		sprintf(pattern2, "QGroupBox, QLabel {"
				"background: rgba(200,200,255,0);"
				"font: bold %dpx;"
				"color: white;"
				"border-width: 0px;"
				//"margin-top: %dpx;"
				"}", sz);//, sz);

		char pattern3[1024];
		sprintf(pattern3, "QSlider::handle:vertical {"
				"background: rgb(200,200,255);"//qradialgradient(cx:0, cy:0, radius: 2,"
					   //"fx:1.0, fy:1.0, stop:0 rgba(0,0,0, 0), stop:1 rgba(200,255,200,250));"
				"border-radius: 4px;"
				"border-style: outset;"
				"border-width: 0px;"
				"border-color: rgba(200,255,200,150);"
				"}"

				"QSlider{"
				"background: rgba(155,100,100,155);"//"background: qradialgradient(cx:0, cy:0, radius: 2,"
				//					   "fx:1.0, fy:1.0, stop:0 rgba(0,0,0, 0), stop:1 rgba(200,255,200,250));"
				"border-radius: 8px;"
				"border-style: outset;"
				"border-width: 0px;"
				"border-color: rgba(200,255,200,150);"
				"color: white;"
				"font: bold %dpx;"
				"}"

				"QSlider::handle:horizontal {"
				"background: rgb(200,200,255);"//qradialgradient(cx:0, cy:0, radius: 2,"
				//	   //"fx:1.0, fy:1.0, stop:0 rgba(0,0,0, 0), stop:1 rgba(200,255,200,250));"
				"border-radius: 4px;"
				"border-style: outset;"
				"border-width: 0px;"
				"border-color: rgba(200,255,200,150);"
				"}",

				sz);

		char style[4096];

		sprintf(style,"%s%s%s", pattern, pattern2, pattern3);
//		sprintf(style+strlen(style),"QLabel%s", pattern2);
//		sprintf(style+strlen(style),"QComboBox%s", pattern);
//		sprintf(style+strlen(style),"QLineEdit%s", pattern);
//		sprintf(style+strlen(style),"QCheckBox%s", pattern);
//		sprintf(style+strlen(style),"QSpinBox%s", pattern);
//		sprintf(style+strlen(style),"QPushButton%s", pattern);
//		sprintf(style+strlen(style),"QGroupBox%s", pattern2);
//		sprintf(style+strlen(style),"QDoubleSpinBox%s", pattern);
//		sprintf(style+strlen(style),"QTimeEdit%s", pattern);
//		sprintf(style+strlen(style),"QListWidget%s", pattern);
//		sprintf(style+strlen(style),"QToolButton%s", pattern);
//		sprintf(style+strlen(style),"QRadioButton%s", pattern);

		ExtendedList* parent = (ExtendedList*)parentWidget();
		parent->setStyleSheet(style);
		userData->setStyleSheet(style);

//		if(!strcmp(userData->metaObject()->className(), "QGroupBox"))
//		{
//			QGroupBox* b = (QGroupBox*)userData;
//			b->layout()->setContentsMargins(0, 50, 0, 0);
//		}
	}

	return 0;
}

ExtendedList::ExtendedList(intf_thread_t *_p_intf, QBoxLayout::Direction itemDir, QBoxLayout::Direction panelDir, int ratio, QWidget* parent, unsigned char opacity)
:QWidget(parent)
,p_intf(_p_intf)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	currentPanel = 0;

	mItemsLayout = new QBoxLayout(itemDir);
	mItemsLayout->setSpacing(0);
	mItemsLayout->setMargin(0);

	panelLayout = new QVBoxLayout;
	panelLayout->setSpacing(0);
	panelLayout->setMargin(0);

	int border_width = opacity?1:0;
	char style[1024];
	sprintf(style, "background: qradialgradient(cx:0, cy:0, radius: 2,"
						   "fx:1.0, fy:1.0, stop:0 rgba(0,0,0, 0), stop:1 rgba(200,255,200,%u));"
					"border-radius: 8px;"
					"border-style: outset;"
					"border-width: %dpx;"
					"border-color: rgba(200,255,200,150);"
					"}", opacity, border_width);

	QWidget* panel = new QWidget(this);
	panel->setLayout(panelLayout);
	panel->setStyleSheet(style);

    scroll = new QScrollArea;
    scroll->setWidgetResizable( true );
    scroll->setWidget( panel );
    scroll->setStyleSheet("background-color:rgb(255, 255, 255, 0);"
			"border-style: outset;"
			    "border-width: 0px;"
			    "border-color: beige;"
    		"font: 15px");

    QFont f = font();
	int i_font_size = var_InheritInteger( p_intf, "qt-font-size" );
	fprintf(stderr, "%s:%s:%d:i_font_size:%d\n", __FILE__, __FUNCTION__, __LINE__, i_font_size);
	f.setPointSize(20);

	scroll->setFont(f);
	panel->setFont(f);

	mainLayout = new QBoxLayout(itemDir);
	mainLayout->setSpacing(0);
	mainLayout->setMargin(0);
	mainLayout->addWidget(scroll, ratio*2);

	QBoxLayout* l = new QBoxLayout(panelDir);
	l->setSpacing(0);
	l->setMargin(0);
	l->addLayout(mItemsLayout, 1);
	l->addLayout(mainLayout, ratio);

	setLayout(l);

	setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);

}

int ExtendedList::addItem(ExtendedListItem* item)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	mItemsLayout->addWidget(item);

	return 0;
}

int ExtendedList::addToMainLayout(QWidget* w, int spacing)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	mainLayout->addWidget(w, spacing);
	return 0;
}

int ExtendedList::addToMainLayout(QBoxLayout* l, int spacing)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	mainLayout->addLayout(l, spacing);
	return 0;
}

int ExtendedList::onItemSelect(ExtendedListItem* item)
{
#if EXTENDED_LIST_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if(currentPanel && currentPanel->userData)
	{
		panelLayout->removeWidget(currentPanel->userData);
		currentPanel->userData->hide();
		currentPanel->unselect();
	}

	if(item->userData)
	{
		unsigned char opacity = 0;
		if(strcmp(item->userData->metaObject()->className(), "ExtendedList"))
		{
			opacity = 100;
		}

		int border_width = opacity?1:0;
		char style[1024];
		sprintf(style, "background: qradialgradient(cx:0, cy:0, radius: 2,"
							   "fx:1.0, fy:1.0, stop:0 rgba(0,0,0, 0), stop:1 rgba(200,255,200,%u));"
						"border-radius: 8px;"
						"border-style: outset;"
						"border-width: %dpx;"
						"border-color: rgba(200,255,200,150);"
						"}", opacity, border_width);

		scroll->widget()->setStyleSheet(style);
		//setStyleSheet(style);

		panelLayout->addWidget(item->userData);
		currentPanel = item;
		currentPanel->userData->show();
		currentPanel->select();

		float pfs = ((float)scroll->width()+(float)scroll->height())/40.;
		currentPanel->resizeFont(pfs);
	}

	return 0;
}

void ExtendedList::resizeEvent(QResizeEvent * event)
{
	if(currentPanel)
	{
		float pfs = ((float)scroll->width()+(float)scroll->height())/40.;
		fprintf(stderr, "%s:%s:%d:panel:%d-%d\n", __FILE__, __FUNCTION__, __LINE__, scroll->width(), scroll->height());
		currentPanel->resizeFont(pfs);
	}

	QWidget::resizeEvent(event);
}
