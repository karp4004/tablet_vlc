/*****************************************************************************
 * open.cpp : Advanced open dialog
 *****************************************************************************
 * Copyright © 2006-2011 the VideoLAN team
 *
 * Authors: Jean-Baptiste Kempf <jb@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "main_interface.hpp" /* Show playlist */

#include "dialogs/extended.hpp"
#include "dialogs/extended_list.hpp"
#include "dialogs/extended_tool.hpp"

#include "dialogs/open.hpp"
#include "dialogs_provider.hpp"
#include "recents.hpp"
#include "util/qt_dirs.hpp"

#include <QTabWidget>
#include <QRegExp>
#include <QMenu>

#define OPEN_STACK_TRACE 1

#ifndef NDEBUG
# define DEBUG_QT 1
#endif

OpenDialog *OpenDialog::instance = NULL;

OpenDialog* OpenDialog::getInstance( QWidget *parent, intf_thread_t *p_intf,
        bool b_rawInstance, int _action_flag, bool b_selectMode, bool _b_pl )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    /* Creation */
    if( !instance )
        instance = new OpenDialog( parent, p_intf, b_selectMode,
                                   _action_flag, _b_pl );
    else if( !b_rawInstance )
    {
        /* Request the instance but change small details:
           - Button menu */
        if( b_selectMode )
            _action_flag = SELECT; /* This should be useless, but we never know
                                      if the call is correct */
        instance->setWindowModality( Qt::WindowModal );
        instance->i_action_flag = _action_flag;
        instance->b_pl = _b_pl;
        instance->setMenuAction();
    }
    return instance;
}

OpenDialog::OpenDialog( QWidget *parent,
                        intf_thread_t *_p_intf,
                        bool b_selectMode,
                        int _action_flag,
                        bool _b_pl)  :  QVLCWidget( _p_intf )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	QFont font;

    ExtendedList* lst = new ExtendedList(_p_intf, QBoxLayout::TopToBottom, QBoxLayout::LeftToRight, 3, this);

    /* Tab definition and creation */
    fileOpenPanel    = new FileOpenPanel( this, p_intf );
    discOpenPanel    = new DiscOpenPanel( this, p_intf );
    netOpenPanel     = new NetOpenPanel( this, p_intf );
    captureOpenPanel = new CaptureOpenPanel( this, p_intf );
    captureOpenPanel->initialize();

    QWidget* extended_tab = new QWidget( this );
    ui_exended.setupUi( extended_tab );

	ExtendedListItem* i1 = new ExtendedListItem(_p_intf, "File", font, "/usr/local/share/qml/images/apple/a1.png", fileOpenPanel, lst);
	ExtendedListItem* i2 = new ExtendedListItem(_p_intf, "Disk", font, "/usr/local/share/qml/images/apple/a1.png", discOpenPanel, lst);
	ExtendedListItem* i3 = new ExtendedListItem(_p_intf, "Network", font, "/usr/local/share/qml/images/apple/a1.png", netOpenPanel, lst);
	ExtendedListItem* i4 = new ExtendedListItem(_p_intf, "Capture device", font, "/usr/local/share/qml/images/apple/a1.png", captureOpenPanel, lst);
	ExtendedListItem* i5 = new ExtendedListItem(_p_intf, "Extended options", font, "/usr/local/share/qml/images/apple/a1.png", extended_tab, lst);

    playButton = new QToolButton(lst);
    playButton->setText(qtr( "&Play" ));
    cancelButton = new QPushButton( qtr( "&Cancel" ) );
    playButton->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );
    cancelButton->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );

    /* Menu for the Play button */
    QMenu * openButtonMenu = new QMenu( "Open" );
    openButtonMenu->addAction( qtr( "&Enqueue" ), this, SLOT( enqueue() ),
                                    QKeySequence( "Alt+E" ) );
    openButtonMenu->addAction( qtr( "&Play" ), this, SLOT( play() ),
                                    QKeySequence( "Alt+P" ) );
    openButtonMenu->addAction( qtr( "&Stream" ), this, SLOT( stream() ) ,
                                    QKeySequence( "Alt+S" ) );
    openButtonMenu->addAction( qtr( "C&onvert" ), this, SLOT( transcode() ) ,
                                    QKeySequence( "Alt+O" ) );

    playButton->setMenu( openButtonMenu );

    QHBoxLayout* buttons_layout = new QHBoxLayout();
    buttons_layout->setSpacing(0);
    buttons_layout->setMargin(0);
    buttons_layout->addWidget(playButton);
    buttons_layout->addWidget(cancelButton);

    QVBoxLayout* right_layout = new QVBoxLayout();
    right_layout->addWidget(lst,5);
    //right_layout->addLayout(buttons_layout,1);
    right_layout->setSpacing(0);
    right_layout->setMargin(0);

    lst->addToMainLayout(buttons_layout, 1);

	setLayout(right_layout);


    /* Buttons action */
    BUTTONACT( playButton, selectSlots() );
    BUTTONACT( cancelButton, cancel() );

    CONNECT( fileOpenPanel, mrlUpdated( const QStringList&, const QString& ),
             this, updateMRL( const QStringList&, const QString& ) );
	return;
}

/* Finish the dialog and decide if you open another one after */
void OpenDialog::setMenuAction()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    if( i_action_flag == SELECT )
    {
        playButton->hide();
    }
    else
    {
        switch ( i_action_flag )
        {
        case OPEN_AND_STREAM:
            playButton->setText( qtr( "&Stream" ) );
            break;
        case OPEN_AND_SAVE:
            playButton->setText( qtr( "C&onvert / Save" ) );
            break;
        case OPEN_AND_ENQUEUE:
            playButton->setText( qtr( "&Enqueue" ) );
            break;
        case OPEN_AND_PLAY:
        default:
            playButton->setText( qtr( "&Play" ) );
        }
        playButton->show();
    }
}

OpenDialog::~OpenDialog()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    getSettings()->setValue( "OpenDialog/size", size() -
                 ( ui.advancedFrame->isEnabled() ?
                   QSize(0, ui.advancedFrame->height()) : QSize(0, 0) ) );
    getSettings()->setValue( "OpenDialog/advanced", ui.advancedFrame->isVisible() );
}

/* Used by VLM dialog and inputSlave selection */
QString OpenDialog::getMRL( bool b_all )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    if( itemsMRL.count() == 0 ) return "";
    return b_all ? itemsMRL[0] + getOptions()
                 : itemsMRL[0];
}

QString OpenDialog::getOptions()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    return ui_exended.advancedLineInput->text();
}

void OpenDialog::showTab( int i_tab )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    if( i_tab == OPEN_CAPTURE_TAB ) captureOpenPanel->initialize();
    ui.Tab->setCurrentIndex( i_tab );
    show();
    if( ui.Tab->currentWidget() != NULL )
    {
        OpenPanel *panel = qobject_cast<OpenPanel *>( ui.Tab->currentWidget() );
        assert( panel );
        panel->onFocus();
    }
}

void OpenDialog::toggleAdvancedPanel()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    if( ui.advancedFrame->isVisible() )
    {
        ui.advancedFrame->hide();
        ui.advancedFrame->setEnabled( false );
        if( size().isValid() )
            resize( size().width(), size().height()
                    - ui.advancedFrame->height() );
    }
    else
    {
        ui.advancedFrame->show();
        ui.advancedFrame->setEnabled( true );
        if( size().isValid() )
            resize( size().width(), size().height()
                    + ui.advancedFrame->height() );
    }
}

void OpenDialog::browseInputSlave()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    OpenDialog *od = new OpenDialog( this, p_intf, true, SELECT );
    //od->exec();
    ui.slaveText->setText( od->getMRL( false ) );
    delete od;
}

/* Function called on signal currentChanged triggered */
void OpenDialog::signalCurrent( int i_tab )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    if( i_tab == OPEN_CAPTURE_TAB ) captureOpenPanel->initialize();
    if( ui.Tab->currentWidget() != NULL )
    {
        OpenPanel *panel = qobject_cast<OpenPanel *>( ui.Tab->currentWidget() );
        assert( panel );
        panel->onFocus();
        panel->updateMRL();
    }
}

/***********
 * Actions *
 ***********/
/* If Cancel is pressed or escaped */
void OpenDialog::cancel()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    /* Clear the variables */
    itemsMRL.clear();
    optionsMRL.clear();

    if(fileOpenPanel)
    {
    	fileOpenPanel->clear();
    }

    p_intf->p_sys->p_mi->toggleView(kPlay);
}

/* If EnterKey is pressed */
void OpenDialog::close()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

     selectSlots();
}

/* Play button */
void OpenDialog::selectSlots()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    switch ( i_action_flag )
    {
    case OPEN_AND_STREAM:
        stream();
        break;
    case OPEN_AND_SAVE:
        transcode();
        break;
    case OPEN_AND_ENQUEUE:
        enqueue();
        break;
    case OPEN_AND_PLAY:
    default:
        play();
    }
}

/* Play Action, called from selectSlots or play Menu */
void OpenDialog::play()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    enqueue( false );
}

/* Enqueue Action, called from selectSlots or enqueue Menu */
void OpenDialog::enqueue( bool b_enqueue )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    if( i_action_flag == SELECT )
    {
//        accept();
        return;
    }

//    for( int i = 0; i < OPEN_TAB_MAX; i++ )
//        qobject_cast<OpenPanel*>( ui.Tab->widget( i ) )->onAccept();

    /* Sort alphabetically */
    itemsMRL.sort();
    fprintf(stderr, "%s:%s:%d:MRL:%d\n", __FILE__, __FUNCTION__, __LINE__, itemsMRL.count());

    /* Go through the item list */
    for( int i = 0; i < itemsMRL.count(); i++ )
    {
        bool b_start = !i && !b_enqueue;

        input_item_t *p_input_item;
        p_input_item = input_item_New( qtu( itemsMRL[i] ), NULL );

        /* Take options from the UI, not from what we stored */
        QStringList optionsList = getOptions().split( " :" );

        /* Insert options */
        for( int j = 0; j < optionsList.count(); j++ )
        {
            QString qs = colon_unescape( optionsList[j] );
            if( !qs.isEmpty() )
            {
                input_item_AddOption( p_input_item, qtu( qs ),
                                      VLC_INPUT_OPTION_TRUSTED );
#ifdef DEBUG_QT
                msg_Warn( p_intf, "Input option: %s", qtu( qs ) );
#endif
            }
        }
        playlist_AddInput( THEPL, p_input_item,
                PLAYLIST_APPEND | ( b_start ? PLAYLIST_GO : PLAYLIST_PREPARSE ),
                PLAYLIST_END, b_pl ? true : false, pl_Unlocked );
        vlc_gc_decref( p_input_item );

        /* Do not add the current MRL if playlist_AddInput fail */
        RecentsMRL::getInstance( p_intf )->addRecent( itemsMRL[i] );
    }

    p_intf->p_sys->p_mi->toggleView(kPlay);
}

void OpenDialog::transcode()
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    stream( true );
}

void OpenDialog::stream( bool b_transcode_only )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    QString soutMRL = getMRL( false );
    if( soutMRL.isEmpty() ) return;
    toggleVisible();

    /* Dbg and send :D */
    msg_Dbg( p_intf, "MRL passed to the Sout: %s", qtu( soutMRL ) );
    THEDP->streamingDialog( this, soutMRL, b_transcode_only,
                            getOptions().split( " :" ) );
}

/* Update the MRL items from the panels */
void OpenDialog::updateMRL( const QStringList& item, const QString& tempMRL )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    optionsMRL = tempMRL;
    itemsMRL = item;
    updateMRL();
}

/* Update the complete MRL */
void OpenDialog::updateMRL() {
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    QString mrl = optionsMRL;
    if( ui_exended.slaveCheckbox->isChecked() ) {
        mrl += " :input-slave=" + ui_exended.slaveText->text();
    }
    mrl += QString( " :%1=%2" ).arg( storedMethod ).
                                arg( ui_exended.cacheSpinBox->value() );
    if( ui_exended.startTimeTimeEdit->time() != ui_exended.startTimeTimeEdit->minimumTime() ) {
        mrl += QString( " :start-time=%1.%2" )
                .arg( QString::number(
                    ui_exended.startTimeTimeEdit->minimumTime().secsTo(
                        ui_exended.startTimeTimeEdit->time()
                ) ) )
               .arg( ui_exended.startTimeTimeEdit->time().msec(), 3, 10, QChar('0') );
    }
    ui_exended.advancedLineInput->setText( mrl );
    ui_exended.mrlLine->setText( itemsMRL.join( " " ) );
    /* Only allow action without valid items */
    playButton->setEnabled( !itemsMRL.isEmpty() );
}

/* Change the caching combobox */
void OpenDialog::newCachingMethod( const QString& method )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    if( method != storedMethod ) {
        storedMethod = method;
        int i_value = var_InheritInteger( p_intf, qtu( storedMethod ) );
        ui.cacheSpinBox->setValue( i_value );
    }
}

/* Split the entries
 * FIXME! */
QStringList OpenDialog::SeparateEntries( const QString& entries )
{
#if OPEN_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    bool b_quotes_mode = false;

    QStringList entries_array;
    QString entry;

    int index = 0;
    while( index < entries.count() )
    {
        int delim_pos = entries.indexOf( QRegExp( "\\s+|\"" ), index );
        if( delim_pos < 0 ) delim_pos = entries.count() - 1;
        entry += entries.mid( index, delim_pos - index + 1 );
        index = delim_pos + 1;

        if( entry.isEmpty() ) continue;

        if( !b_quotes_mode && entry.endsWith( "\"" ) )
        {
            /* Enters quotes mode */
            entry.truncate( entry.count() - 1 );
            b_quotes_mode = true;
        }
        else if( b_quotes_mode && entry.endsWith( "\"" ) )
        {
            /* Finished the quotes mode */
            entry.truncate( entry.count() - 1 );
            b_quotes_mode = false;
        }
        else if( !b_quotes_mode && !entry.endsWith( "\"" ) )
        {
            /* we found a non-quoted standalone string */
            if( index < entries.count() ||
                entry.endsWith( " " ) || entry.endsWith( "\t" ) ||
                entry.endsWith( "\r" ) || entry.endsWith( "\n" ) )
                entry.truncate( entry.count() - 1 );
            if( !entry.isEmpty() ) entries_array.append( entry );
            entry.clear();
        }
        else
        {;}
    }

    if( !entry.isEmpty() ) entries_array.append( entry );

    return entries_array;
}

