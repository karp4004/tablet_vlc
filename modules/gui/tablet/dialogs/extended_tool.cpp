#include "dialogs/extended_tool.hpp"
#include "components/extended_panels.hpp"

#include <QFont>
#include <QVBoxLayout>
#include <QToolBar>
#include <QToolButton>
#include <QFont>
#include <QPushButton>
#include <QResizeEvent>
#include <QScrollArea>

#include "stdio.h"
#include "math.h"
#include "tablet.hpp"
#include "vlc_interface.h"
#include <string>

using namespace std;

#define EXTENDED_TOOL_TRACE 0
#define EXTENDED_PAINT 0
#define TOOL_DEBUG 0
#define TOOL_DEBUG_NONTRANSPARENT 0

QToolButton* createToolButton(QToolBar* topBar, QFont font, QString text, QString iconPath, QWidget* panel, bool addSeparator)
{
#if EXTENDED_TOOL_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    QToolButton* empty = new ExtendedToolButton(panel, 0);
    empty->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    empty->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    empty->setFont(font);

    empty->setText(text);
    topBar->addWidget(empty);
    empty->setIcon(QIcon(iconPath));

    if(addSeparator)
    {
    	topBar->addSeparator();
    }

    return empty;
}

ExtendedTool::ExtendedTool(intf_thread_t *_p_intf, QWidget* parent)
:QWidget(parent)
,p_intf(_p_intf)
,currentAudioPanel(0)
{
#if EXTENDED_TOOL_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	currentButton = 0;

    QVBoxLayout* audioSettingsLayout = new QVBoxLayout(this);
    audioSettingsLayout->setSpacing(0);
    audioSettingsLayout->setMargin(0);
	setLayout(audioSettingsLayout);

	//------------bar
	topBar = new QToolBar(this);
	topBar->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    topBar->setMovable(false);
//	topBar->setFrameStyle( QFrame::NoFrame );
    topBar->setStyleSheet("background-color:rgb(255, 255, 255, 0);");

    //----------------panel
    QWidget* audioPanel = new QWidget(this);
    audioPanelLayout = new QVBoxLayout(audioPanel);
    audioPanelLayout->setMargin(0);
    audioPanelLayout->setSpacing(0);
    audioPanel->setStyleSheet("background-color:rgb(255, 255, 255, 0);");

    QScrollArea *scroll = new QScrollArea;
    scroll->setWidgetResizable( true );
    scroll->setWidget( audioPanel );

    audioSettingsLayout->addWidget(topBar, 1);
    audioSettingsLayout->addWidget(scroll, 10);
}

int ExtendedTool::addButton(QString text, QString iconPath, QWidget* panel, bool addSeparator)
{
	QAbstractButton* button = createToolButton(topBar, mFont, text, iconPath, panel, addSeparator);
    mButtons.push_back(button);
    CONNECT( button, clicked(), this, onToolClick() );

#if TOOL_DEBUG
	fprintf(stderr, "%s:%s:%d:i_font_size:%d\n", __FILE__, __FUNCTION__, __LINE__, i_font_size);
#endif

	 QFont f = font();

    if(panel)
    {
    	panel->hide();
    }

    if(button)
    {
    	button->setFont(f);
    }

    if(!currentButton)
    {
    	switchTool(button);
    }

	return 0;
}

int ExtendedTool::switchTool(QObject* sender)
{
#if EXTENDED_TOOL_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	if(currentButton)
	{
		currentButton->setStyleSheet("background-color:rgb(255, 255, 255, 0);");
	}

	currentButton = (ExtendedToolButton*)sender;
	currentButton->setStyleSheet(     "background: qradialgradient(cx:0, cy:0, radius: 1,"
            "fx:1.0, fy:1.0, stop:0 rgba(255,255,255, 150), stop:1 rgba(255,255,255, 150));"
 "border-radius: 4px;"
"border-style: outset;"
"border-width: 2px;"
"border-color: rgba(100,100,255, 150);");

	fprintf(stderr, "%s:%s:%d: button:%p\n", __FILE__, __FUNCTION__, __LINE__, currentButton);
	fprintf(stderr, "%s:%s:%d: currentAudioPanel:%p\n", __FILE__, __FUNCTION__, __LINE__, currentAudioPanel);

	if(currentAudioPanel)
	{
		audioPanelLayout->removeWidget(currentAudioPanel);
		currentAudioPanel->hide();
		currentAudioPanel = 0;
	}

	if(currentButton->mPanel)
	{
		audioPanelLayout->addWidget(currentButton->mPanel);
		currentAudioPanel = currentButton->mPanel;
		currentAudioPanel->show();
	}

	return 0;
}

void ExtendedTool::onToolClick()
{
#if EXTENDED_TOOL_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	switchTool(sender());
}
